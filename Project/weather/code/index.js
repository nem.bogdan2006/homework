const apiKey = '5abd2d02fb7b62a1bb5325614f57abae';

const
    search = document.querySelector('.input__search'),
    btnSearch = document.querySelector('.btn__search'),
    temperature = document.querySelector('.temperature'),
    cityName = document.querySelector('.city'),
    procentOfHumidity = document.querySelector('.procentOfhumudity'),
    windSpeed = document.querySelector('.wind');

btnSearch.addEventListener('click', () => {
    const city = search.value;

    console.log(city);
    if (city) {
        const apiUrl = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}`;

        fetch(apiUrl)
            .then(function (response) {
                if (!response.ok) {
                    throw new Error('Error fetching data for ' + city);
                }
                return response.json();
            })
            .then(function (data) {
                console.log(data);

                temperature.textContent = `${data.main.temp}°C`;
                cityName.textContent = `${data.name}`;
                procentOfHumidity.textContent = ` ${data.main.humidity}%`;
                windSpeed.textContent = `${data.wind.speed} m/s`;

                const weatherImg = document.querySelector('.weather__img');


                const { main } = data.weather[0];

                if (main === 'Clear') {
                    weatherImg.src = './img/clear.png';
                } else if (main === 'Clouds') {
                    weatherImg.src = './img/clouds.png';
                } else if (main === 'Drizzle') {
                    weatherImg.src = './img/drizzle.png';
                } else if (main === 'Mist') {
                    weatherImg.src = './img/mist.png';
                } else if (main === 'Rain') {
                    weatherImg.src = './img/rain.png';
                } else if (main === 'Snow') {
                    weatherImg.src = './img/snow.png';
                }

            })
            .catch(function (error) {
                console.error(error);
            });
    } else {
        console.log('City name not provided.');
    }
});
