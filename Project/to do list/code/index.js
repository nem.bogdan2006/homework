const addBtn = document.querySelector('.add');
const input = document.querySelector('input');
const taskList = document.querySelector('.task-list');

addBtn.addEventListener('click', addTask);

input.addEventListener('keydown', (event) => {
    if (event.key === 'Enter') {
        addTask();
    }
});

function addTask() {
    const taskBox = document.createElement('div');
    taskBox.classList.add('task');

    const buttonBox = document.createElement('div');
    buttonBox.classList.add('buttonBox');

    const removeBtn = document.createElement('button');
    removeBtn.classList.add('remove');
    removeBtn.textContent = 'remove';

    const doneBtn = document.createElement('button');
    doneBtn.classList.add('done');
    doneBtn.textContent = 'done';

    const li = document.createElement('li');
    li.textContent = input.value;

    taskList.appendChild(taskBox);

    taskBox.appendChild(li);
    taskBox.appendChild(buttonBox);

    buttonBox.appendChild(removeBtn);
    buttonBox.appendChild(doneBtn);

    input.value = '';

    removeBtn.addEventListener('click', () => {
        taskList.removeChild(taskBox);
    });
}
