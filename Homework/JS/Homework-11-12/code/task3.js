import city from './uacity.js'


// 3.
// Використовуючи бібліотеку bootstrap створіть форму у якій запитати у користувача данні.
// Ім'я, Прізвище (Українською)
// Список з містами України 
// Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора. 
// Пошта 
// Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅

// Перевіряти данні на етапі втрати фокуса та коли йде натискання кнопки відправити дані 


const [...inputs] = document.getElementsByTagName("input");


const
    namePattern = /^[А-яіїйє-]{2,15}$/,
    phonePattern = /^\+380\d{2} \d{3} \d{2} \d{2}$/,
    emailPattern = /\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/gi;

const data = {
    name: "",
    surname: "",
    city: "",
    phone: "",
    email: ""
}

inputs.forEach((e) => {
    e.addEventListener("change", () => {
        if (namePattern.test(e.value) && e.id === "name") {
            data.name = e.value;
        } else if (namePattern.test(e.value) && e.id === "surname") {
            data.surname = e.value;
        } else if (phonePattern.test(e.value) && e.id === "phone") {
            data.phone = e.value;
        } else if (emailPattern.test(e.value) && e.id === "email") {
            data.email = e.value;
        }
    });
});

const
    phoneNumber = document.querySelector('#phone-number'),
    result = document.createElement('p'),
    form = document.querySelector('#list');


phoneNumber.addEventListener("change", e => {
    if (phonePattern.test(e.target.value)) {
        result.textContent = 'Ви ввели правильний номер телефону✅';
        data.phone = e.target.value;
    } else {
        result.textContent = 'Ви ввели неправильний номер телефону❌';
    }
    form.appendChild(result);
});

document.querySelector('#btn').addEventListener("click", e => {
    console.log(data);
})

city.forEach(e => {
    const option = document.createElement("option");
    document.getElementById('uk').append(option);
    option.value = e.city;
    
});

const ukInput = document.querySelector('#uk-input');

ukInput.addEventListener("change", e => {
    data.city = e.target.value;
});


