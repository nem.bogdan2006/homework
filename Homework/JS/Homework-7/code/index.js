/*
1. Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), 
які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю ку    
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
https://storage.googleapis.com/www.examclouds.com/oop/car-ierarchy.png

*/




class Driver {
    constructor(fullName, driverExperience) {
        this.fullName = fullName;
        this.driverExperience = driverExperience;
    }
}

class Engine {
    constructor(fullName, power) {
        this.fullName = fullName;
        this.power = power;
    }
}

class Car {
    constructor(fullName, brand, weight, power, producer) {
        this.fullName = fullName;
        this.brand = brand;
        this.weight = weight;
        this.power = power;
        this.producer = producer;
    }

    start() {
        return 'Поїхали';
    }

    stop() {
        return 'Зупиняємося';
    }

    turnRight() {
        return 'Поворот праворуч'
    }

    turnLeft() {
        return 'Поворот ліворуч';
    }

    toString() {
        return `Інформація про автомобіль:
  ПІБ водія: ${this.fullName}
  Марка автомобіля: ${this.brand}
  Вага автомобіля: ${this.weight}
  Потужність двигуна: ${this.power}
  Виробник двигуна: ${this.producer}`;
    }
}

class Lorry extends Car {
    constructor(fullName, brand, weight, power, producer, cargoCapacity) {
        super(fullName, brand, weight, power, producer);
        this.cargoCapacity = cargoCapacity;
    }

    toString() {
        return super.toString() + `\nВантажопідйомність кузова: ${this.cargoCapacity}`;
    }
}

class SportCar extends Car {
    constructor(fullName, brand, weight, power, producer, topSpeed) {
        super(fullName, brand, weight, power, producer);
        this.topSpeed = topSpeed;
    }

    toString() {
        return super.toString() + `\nГранична швидкість: ${this.topSpeed}`;
    }
}

const car = new Car("Нестерчук Звенигор Захарович", "Mercedess", "1500 kg", "275 HP", "German Auto");
const car1 = new Car("JЗагороднюк Царко Романович", "Tesla", "2000 kg", "300 HP", "ACME Motors");
const car2 = new Car("Губенко Яртур Остапович", "BMW", "1800 kg", "250 HP", "German Auto");

const lorry = new Lorry("Палій Анастасій Добромирович", "Volvo", "5000 kg", "350 HP", "ACME Motors", "10 tons");
const sportCar = new SportCar("Ніклевич Йонас Тихонович", "Ferrari", "1500 kg", "700 HP", "Italian Sports Cars", "350 km/h");

console.log(car.toString());
console.log(car1.toString());
console.log(car2.toString());

console.log(lorry.toString());
console.log(sportCar.toString());



/*
2. Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.
*/

class Animal {
    constructor(food, location) {
        this.food = food;
        this.location = location;
    }

    makeNoise() {
        console.log('Така тварина спить');
    }

    eat() {
        console.log('Тварина їсть');
    }

    sleep() {
        console.log('Тварина спить');
    }
}

class Dog extends Animal {
    constructor(food, location, breed) {
        super(food, location);
        this.breed = breed;
    }

    makeNoise() {
        console.log('Гав-гав!');
    }
}

class Cat extends Animal {
    constructor(food, location, color) {
        super(food, location);
        this.color = color;
    }

    makeNoise() {
        console.log('Мяу-мяу!');
    }
}

class Horse extends Animal {
    constructor(food, location, speed) {
        super(food, location);
        this.speed = speed;
    }

    makeNoise() {
        console.log('Хррррр!');
    }
}



class Veterinarian {
    treatAnimal(animal) {
      console.log(`Їжа: ${animal.food}`);
      console.log(`Місце: ${animal.location}`);
    }
  }
  
  function main() {
    const dog = new Dog('Корм', 'В будинку', 'Лабрадор');
    const cat = new Cat('Риба', 'На горищі', 'Сірий');
    const horse = new Horse('Сіно', 'На фермі', 'Швидкість 60 км/год');
  
    const veterinarian = new Veterinarian();
  
    const animals = [dog, cat, horse];
  
    for (let i = 0; i < animals.length; i++) {
      veterinarian.treatAnimal(animals[i]);
    }
  }
  
  main();



/*
2. Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.
*/