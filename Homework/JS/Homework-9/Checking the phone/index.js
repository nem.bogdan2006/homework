const container = document.querySelector('.container');

const
    input = document.createElement('input'),
    btnSubmit = document.createElement('input');


input.setAttribute('type', 'text');
input.setAttribute('placeholder', 'Введіть номер телефону:');

btnSubmit.setAttribute('type', 'submit');

container.appendChild(input);
container.appendChild(btnSubmit);


const checkNumbers = /^0\d{2}-\d{3}-\d{2}-\d{2}$/;



btnSubmit.onclick = () => {
    if (checkNumbers.test(input.value)) {
        setTimeout(() => { document.location = "https://www.w3schools.com/jsref/jsref_regexp_test.asp" }, 3000)

    } else {
        const errorMessage = document.createElement('p');
        errorMessage.textContent = 'Ви ввели неправильний номер телефону';
        container.appendChild(errorMessage);
    }
}




