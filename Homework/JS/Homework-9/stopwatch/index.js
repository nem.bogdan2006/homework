const
    // buttons
    btnStart = document.querySelector('#btn-start'),
    btnStop = document.querySelector("#btn-stop"),
    btnReset = document.querySelector("#btn-reset"),

    containerStopwatch = document.querySelector('.container-stopwatch');
// buttons

// stopwathch dispay
let
    hour = document.querySelector('#hour'),
    minute = document.querySelector('#minute'),
    sec = document.querySelector('#sec');
// stopwathch dispay


let
    timerSec = 0,
    timerMinute = 0,
    timerHour = 0,

    secInterval = null,
    minInterval = null,
    hourInterval = null;



let isTimerRunning = false;


const beautifulTime = time => {
    if (time < 10) {
        return '0' + time;
    }

    return time;
};


const showTime = (time, element, type = 'second', max = 59) => {
    if (time === max) {
        time = -1;
    }
    time++;

    switch (type) {
        case 'minute':
            timerMinute = time;

            break;
        case 'hour':
            timerHour = time;
            break;

        default:
            timerSec = time;

    }

    element.textContent = beautifulTime(time);
};

const bacgroundClear = () => {
    containerStopwatch.classList.remove('black')
    containerStopwatch.classList.remove('silver')
    containerStopwatch.classList.remove('red')
    containerStopwatch.classList.remove('green')
}


const timerStop = (sec, min, hour) => {
    bacgroundClear();
    containerStopwatch.classList.add('red');


    clearInterval(sec);
    clearInterval(min);
    clearInterval(hour);


    btnStart.disabled = false;
};

const timerReset = () => {
    bacgroundClear();
    containerStopwatch.classList.add('silver');

    clearInterval(secInterval);
    clearInterval(minInterval);
    clearInterval(hourInterval);

    btnStart.disabled = false;

    sec.textContent = '00';
    minute.textContent = '00';
    hour.textContent = '00';

    timerSec = 0;
    timerMinute = 0;
    timerHour = 0;
};


btnStart.onclick = () => {
        bacgroundClear();
    containerStopwatch.classList.add('green');

    secInterval = setInterval(() => showTime(timerSec, sec), 1000);
    minInterval = setInterval(() => showTime(timerMinute, minute, 'minute'), 60000);
    hourInterval = setInterval(() => showTime(timerHour, hour, 'hour', 24), 3600000);


    btnStart.disabled = true;
};

btnStop.onclick = () => timerStop(secInterval, minInterval, hourInterval);
btnReset.onclick = () => timerReset(secInterval, minInterval, hourInterval);


