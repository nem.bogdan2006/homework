const res = new XMLHttpRequest();

res.open('get', 'https://swapi.dev/api/people/?format=json');

res.send();

res.addEventListener('readystatechange', () => {
    if (res.readyState == 4 && res.status >= 200 && res.status < 300) {
        const response = JSON.parse(res.response);
        const results = response.results;

        const cardsMenu = document.querySelector('.cards'); 

        results.forEach(character => {
            const 
                nameCharacter = character.name,
                genderCharacter = character.gender,
                heightCharacter = character.height,
                colorSkinCharacter = character.skin_color,
                dateOfBirthCharacter = character.birth_year,
                planet = character.homeworld;

            const card = `
            <div class="card">
                <p><span>Name: </span> ${nameCharacter}</p>
                <p><span>Gender: </span> ${genderCharacter}</p>
                <p><span>Height: </span> ${heightCharacter} cm</p>
                <p><span>Color of skin: </span> ${colorSkinCharacter}</p>
                <p><span>Date of bithday: </span> ${dateOfBirthCharacter}</p>
                <a href ="${planet}"><span>Planet: </span> link</a>
            </div>
            `;

            cardsMenu.insertAdjacentHTML('beforeend', card);
        });
    }
});
