const cards = document.querySelector('.cards');
const li = document.querySelectorAll('li');
const loader = document.querySelector('.lds-hourglass');

li.forEach(el => {
    el.addEventListener('click', () => {
        cards.innerHTML = '';

        loader.classList.add('active');
        if (el.textContent.toLowerCase() === 'character') {
            const data = new XMLHttpRequest();
            data.open('get', 'https://rickandmortyapi.com/api/character');
            data.send();
            data.addEventListener('readystatechange', () => {
                if (data.readyState === 4 && data.status >= 200 && data.status < 300) {
                    loader.classList.remove('active');

                    const dataInfo = JSON.parse(data.response);
                    const characters = dataInfo.results;

                    characters.forEach(character => {
                        createCardOfCharacter(character);
                    });
                }
            });
        } else if (el.textContent.toLowerCase() === 'location') {
            fetch("https://rickandmortyapi.com/api/location")
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }

                    return response.json();
                })
                .then(dataInfo => {
                    loader.classList.remove('active');
                    const planets = dataInfo.results;
                    planets.forEach(planet => {
                        createCardOfLocation(planet);
                    });
                })
                .catch(error => {
                    console.error('Fetch error:', error);
                });
        } else if (el.textContent.toLocaleLowerCase() === 'episode') {
            fetch('https://rickandmortyapi.com/api/episode')
            .then(response => {
                if(!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(dataInfo => {
                loader.classList.remove('active');
                const episode = dataInfo.results;
                episode.forEach( episode => {
                    createCardOfEpisode(episode);
                })
            })
        }
    });
});

function createCardOfCharacter(info) {
    const
        div = document.createElement('div'),
        img = document.createElement('img'),
        name = document.createElement('p'),
        planet = document.createElement('p');

    div.classList.add('card');

    img.src = info.image;

    name.textContent = info.name;
    name.classList.add('text__card')

    planet.textContent = info.origin.name;
    planet.classList.add('planet')

    cards.append(div);
    div.append(img);
    div.append(name);
    div.append(planet);
}

function createCardOfLocation(info) {
    const
        div = document.createElement('div'),
        name = document.createElement('p'),
        type = document.createElement('p'),
        dimension = document.createElement('p');

    div.classList.add('card__location');

    name.textContent = info.name;
    type.textContent = info.type;
    dimension.textContent = info.dimension;

    name.classList.add('text__card');
    type.classList.add('type');
    dimension.classList.add('planet');


    div.append(name);
    div.append(type);
    div.append(dimension);

    const cards = document.querySelector('.cards');
    cards.appendChild(div);
}


function createCardOfEpisode(info) {
    const 
        div = document.createElement('div'),
        name = document.createElement('div'),
        air_date = document.createElement('div'),
        episode = document.createElement('div');

    div.classList.add('card__episode');

    name.textContent = info.name;
    air_date.textContent = info.air_date;
    episode.textContent = info.episode;

    name.classList.add('text__card');
    air_date.classList.add('type');
    episode.classList.add('planet');



    div.append(name);
    div.append(air_date);
    div.append(episode);

    const cards = document.querySelector('.cards');
    cards.appendChild(div);
}
