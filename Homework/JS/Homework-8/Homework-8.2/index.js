// Завдання №1

const
    firstInput = document.querySelector('#firstInput'),
    secondInput = document.querySelector('#secondInput'),
    btn = document.querySelector('#btn');


btn.onclick = () => {
    const temp = firstInput.value;
    firstInput.value = secondInput.value;
    secondInput.value = temp;
}

// Завдання №1

// Завдання №2

const divs = document.getElementsByTagName('div');
const square = document.querySelector('.square')


Array.from(divs).forEach((square) => {
    square.onclick = () => {
        square.style.backgroundColor = `rgb(${randomNum(255)}, ${randomNum(255)}, ${randomNum(255)})`;
    };
});

function randomNum(num) {
    return Math.floor(Math.random() * (num + 1));
}
// Завдання №2

// Завдання №3

const
    info = document.querySelector("#div"),
    btn3 = document.querySelector('#btn3'),
    input = document.querySelector('#thirdInput'),
    box3 = document.querySelector('.box3');




if (btn3 && input) {
    let newDiv = null;

    input.addEventListener('input', evt => info.innerHTML = evt.target.value);
    btn3.onclick = () => {
        if (document.body.contains(newDiv)) {
            newDiv.remove();
        } else {
            newDiv = document.createElement('div');
            newDiv.className = 'new__div';
            box3.appendChild(newDiv);
            newDiv.innerText = input.value;
        }

        input.value = '';
    };
}


// Завдання №3 

// Завдання №4 

const
    img1 = document.querySelector('#img1'),
    changeImage = document.querySelector('#chande-image'),
    box4 = document.querySelector('.box4');


let img_i = 1

changeImage.onclick = () => {
    switch (img_i) {
        case 1:
            img1.src = './img/img2.jpg';
            img_i = 2;
            break;

        case 2:
            img1.src = './img/img3.jpg';
            img_i = 3;
            break;

        default:
            img1.src = './img/img1.jpg';
            img_i = 1;
    }
};


// Завдання №4 





// Завдання №5

const btnParagraph = document.querySelector('.paragraph-btn')

btnParagraph.onclick = () => {
    for (let i = 0; i < 10; i++) {
        const paragraph = document.createElement('p');
        document.body.appendChild(paragraph);
        paragraph.innerText = `${i + 1}`;
        paragraph.onclick = () => {
            paragraph.remove();
        };
    }

}



// Завдання №5

