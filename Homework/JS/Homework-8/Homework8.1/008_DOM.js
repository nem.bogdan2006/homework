const
    ul = document.querySelector('#list');

let
    selected_element = document.querySelector('#list').firstChild,
    index = ul.children.length;

const
    selectFirstChild = () => clearAndSelect(ul.firstElementChild),
    selectLastChild = () => clearAndSelect(ul.lastElementChild),
    selectNextNode = () => {
        if (selected_element !== ul.lastElementChild) { 
            clearAndSelect(selected_element.nextElementSibling); 
        } else {
            console.error('Наступний елемент не існує');
        }
    },
    selectPrevNode = () => {
        if (selected_element !== ul.firstElementChild) {
            clearAndSelect(selected_element.previousElementSibling)
        } else {
            console.error('Попередній елемент не існує');
        }
    },
    createNewChild = () => {
        const
            li = document.createElement('li');

        li.innerHTML = `List item ${index + 1}`;

        ul.appendChild(li);

        index = ul.children.length;
    },
    createNewChildAtStart = () => {
        const
            li = document.createElement('li');

        li.innerHTML = `List item ${index + 1}`;;

        ul.prepend(li);

        index = ul.children.length;
    },
    removeLastChild = () =>  {
        if (ul.lastElementChild) {
            ul.lastElementChild.remove();
        } else {
            console.error('Елементів не існує');
        }
    }
    clearAndSelect = el => {
        if (el) {
            selected_element = el;

            ul.querySelectorAll('li').forEach(item => item.style.color = '#000000');
            el.style.color = 'green';
        } else {
            console.error('Елементів не існує');
        }
    };