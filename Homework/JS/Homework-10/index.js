window.addEventListener("DOMContentLoaded", function () {
    const
        numberButtons = document.querySelectorAll('.number'),
        // Buttons
        mrcBtn = document.querySelector('#mrc__btn'),
        mMinusBtn = document.querySelector('#m-minus__btn'),
        mPlusBtn = document.querySelector('#m-plus__btn'),
        dot = document.querySelector('#dot'),
        clearAll = document.querySelector('#clear-all'),
        result = document.querySelector('#result'),
        // sign
        plus = document.querySelector(".plus"),
        minus = document.querySelector(".minus"),
        divide = document.querySelector(".divide"),
        times = document.querySelector(".times"),
        // Screen 
        screen = document.querySelector('#input');
        tmp = this.document.querySelector('#tmp')

    let
        total = 0,
        action = '';



    numberButtons.forEach(numberButton => {
        numberButton.addEventListener("click", () => {
            if (!(numberButton.classList.contains('zero') && !screen.textContent) && action !== 'result') {
                screen.textContent += numberButton.textContent;
            }
        });
    });

    let clicked = false;
    dot.addEventListener("click", () => {
        if (!clicked) {
            screen.textContent += dot.textContent;
            clicked = true;
        }
    });

    minus.addEventListener("click", () => {
        mathematicAction('minus');
    });

    plus.addEventListener("click", () => {
        mathematicAction('plus');
    });

    divide.addEventListener("click", () => {
        mathematicAction('divide')
    });

    times.addEventListener("click", () => {
        mathematicAction('times')
    });

    function mathematicAction(act) {
        if( act ) {
            console.log(screen.textContent);
            console.log(total);
            if (total === 0) {
                total = +screen.textContent;
            } else if( action ) {
                switch (action) {
                    case 'plus': total += +screen.textContent; break;
                    case 'minus': total -= +screen.textContent; break;
                    case 'divide': total /= +screen.textContent; break;
                    case 'times': total *= +screen.textContent; break;
                    default: total = +screen.textContent;
                }
            } else {
                switch (act) {
                    case 'plus': total += +screen.textContent; break;
                    case 'minus': total -= +screen.textContent; break;
                    case 'divide': total /= +screen.textContent; break;
                    case 'times': total *= +screen.textContent; break;
                    default: total = +screen.textContent;
                }
            }

            if( act === 'result' )
            { 
                screen.textContent = total;
            } else
            {
                screen.textContent = "";
            }

            action = `${act}`;

            clicked = false;
            result.disabled = false;
        }

    }



    result.addEventListener("click", () => {
        
        mathematicAction( 'result' );

        // console.log(total);
        // console.log(screen.textContent);
    });


    clearAll.addEventListener("click", () => {
        screen.textContent = "";

        clicked = false;
        showResult = false;
        action = '';
    })


    let memory = [];
    
    mPlusBtn.addEventListener("click", () => {
        memory.push(screen.textContent);
        tmp.textContent = 'm';
    });

    mMinusBtn.addEventListener("click", () => {
        memory.splice(0, 1)
    });

    let clickCount = 0;
    mrcBtn.addEventListener("click", () => {
        if (clickCount === 0) {
            screen.textContent = memory;
        } else {
            memory = [];
            screen.textContent = '';
            tmp.textContent = '';
            clickCount = 0;
        }
        clickCount++;
    });
});