

// 1. Створіть об'єкт заробітної плати obj. Виведіть на екран зарплату Петі та Колі.


// var obj = { 'Коля': '1000', 'Вася': '500', 'Петя': '200' };

// document.write(obj.Петя + '</br>' + obj.Коля);


// 2. Виведіть на сторінку назву валюти ціну купівлі та ціну продажу.
//  https://api.privatbank.ua/p24api/exchange_rates?json&date=11.01.2021.
//   Для виводу використовуєте html таблиці та стилі. переберіть обєкти викортстовуючи властивість values.


// console.log(data.exchangeRate);


// let tbody = document.getElementById('tbody');

// let table = ``

// data.exchangeRate.forEach(function (item, index) {
//     table += `<tr><td>${index + 1}</td><td>${item.currency}</td><td>${item.purchaseRateNB}</td><td>${item.saleRateNB}</td></tr>`
// })

// tbody.innerHTML = table;


// 3. Створіть об'єкт cryptoWallet. У гаманці має зберігатись ім'я власника, кілька валют Bitcoin, Ethereum, Stellar і в кожній валюті додатково є ім'я валюти, логотип,
//  кілька монет та курс на сьогоднішній день день. Також в об'єкті гаманець є метод при виклику якого він приймає ім'я валюти та виводить на сторінку інформацію.
//   "Доброго дня, ... ! На вашому балансі (Назва валюти та логотип) залишилося N монет, якщо ви сьогодні продасте їх те, отримаєте ...грн.


// const cryptoWallet = {
//     Name: 'John Bullet',
//     Bitcoin: {
//         coinName: 'Bitcoin',
//         logo: '<img src="./img/Bitcoin-logo.jpg" alt="Bitcoin">',
//         coinBalance: 1.76,
//         cost: 28476.43

//     },
//     Ethereum: {
//         coinName: 'Ethereum',
//         logo: `<img src="./img/Ethereum-logo.png" alt="Ethereum" >`,
//         coinBalance: 0.53,
//         cost: 1794.78
//     },
//     Stellar: {
//         coinName: 'Stellar',
//         logo: `<img src="./img/Stellar-logo.png" alt="Stellar" >`,
//         coinBalance: 215,
//         cost: 0.097
//     },

//     show: function (coin) {
//         document.write(`Доброго дня, ${cryptoWallet.Name} ! На вашому балансі ${this[coin].coinName} ${this[coin].logo} залишилося ${this[coin].coinBalance} монет,
//          якщо ви сьогодні продасте їх те, отримаєте ${(this[coin].coinBalance * this[coin].cost * 40).toFixed(2)}  грн.`)
//     }
// }

// cryptoWallet.show(prompt("Виберіть монету: Bitcoin, Ethereum, Stellar"));

// 4. Даний рядок типу 'var_text_hello'. Зробіть із нього текст 'VarTextHello'.


// const str = 'var_text_hello';

// const words = str.split('_');

// for (let i = 0; i < words.length; i++) {
//     const word = words[i];
//     words[i] = word.charAt(0).toUpperCase() + word.slice(1);
//   }

// const result = words.join('');

// console.log(result);




// 5. Напишіть функцію isEmpty(obj), яка повертає true, якщо об'єкт не має властивостей, інакше false.


// function isEmpty(obj1) {
//     for (let key in obj) {
//         if (obj.hasOwnProperty(key)) {
//             return false;
//         }
//     }
//     return true;
// }
// const obj1 = {};

// document.write(isEmpty(obj1));






// 6. Зробіть функцію, яка рахує та виводить кількість своїх викликів.

function callCounter() {
    let count = 0;
    return function() {
        count++;
        document.write('Кількість викликів ' + count + '</br>');
    }
}

let counter = callCounter();

counter();
counter();
counter();

