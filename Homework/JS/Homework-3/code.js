// Перше завдання

const letters = ["a", "b" ,"c"];
const numbers = letters.concat(1, 2, 3);

document.write(numbers + "</br>");


// Друге завдання

letters.push(1, 2, 3);

console.log(letters);
document.write(letters + '</br>');

// Третє завдання



const newNumbers = [1, 2, 3];

newNumbers.reverse();

document.write(newNumbers + '</br>')

// Четверте Завдання

newNumbers.reverse();
newNumbers.push(4, 5, 6);

document.write(newNumbers + '</br>')

// П'яте завдання

const newNumbers2 = [1, 2, 3];

newNumbers2.unshift(4, 5, 6);
document.write(newNumbers2 + '</br>');

// Шосте завдання

const a = ['js', 'css', 'jq'];

document.write(a[0] + '</br>');

// Сьоме заваднання

const b = [1, 2, 3, 4, 5];


const new__b = b.slice(0, 3);

document.write(new__b + '</br>')

// Восьме завдання

const question8 = [1, 2, 3, 4, 5];

question8.splice(1, 2);

document.write(question8 + "<br>");

// Дев'яте завадння

const question9 = [1, 2, 3, 4, 5];

question9.splice(2, 0, 10);

document.write(question9 + "</br>");

// Десяте завадння

const question10 = [3, 4, 1, 2, 7];

question10.sort();
document.write(question10 + "</br>");

// Одинадцяте завдання

const question11 = ['Привіт, ', 'світ', '!'];

question11.splice(1, 1, "мир");

document.write(question11.join("") + "<br>");

// Дванадцяте завдання

const question12 = ['Привіт, ', 'світ', '!'];

question12[0] = "Поки";

document.write(question12.join(" ") + "</br>");

// Тринадцяте завдання

const arr = [1, 2, 3, 4, 5];

const arr2 = new Array(1, 2, 3, 4, 5);


// Чотирнадцяте завдання

var arr13 = {
    'ru':['блакитний', 'червоний', 'зелений'],
    'en':['blue', 'red', 'green'],
};

const wordRu = arr13.ru[0];
const wordEn = arr13.en[0];

document.write(wordRu + " "+ wordEn + "</br>");

// П'ятнадцяте завдання

const question14 = ['a', 'b', 'c', 'd'];

const newquestion14 = 'a' + "+" + 'b' + ', ' + 'c' + '+' + 'd'+ '</br>'; 
document.write(newquestion14);

// 16 - 17 завдання

const numElement = parseInt(prompt("Введіть кількість елементів массиву: "))

const arr16 = [];

for (let i = 0; i < numElement; i++ ) {
    arr16.push(i + 1);
}

for (let j = 0; j < arr16.length; j++) {
    if(arr16[j] % 2 === 0) {
        document.write('<p style="background-color: red;">' + arr16[j] + '</p>');
    } else {
         document.write('<p>' + arr16[j] + '</p>');
    }
}

document.write(arr16);


// Вісімнадцяте завдання

const vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морквина'];
const str1 = vegetables.join(', ');

document.write(str1);



