import React from "react";
import { Icon } from '@iconify/react';
import "./user-info.css";


function User() {
    return (
        <>
            <div className="userinfo">
                <Icon icon="ph:user-bold" className="user-icon" />
                <div className="user-text">
                    <p className="user-name">AnimatedFred</p>
                    <p className="user-email">Animated@demo.com</p>
                </div>
            </div>
        </>
    );
}

export default User;

