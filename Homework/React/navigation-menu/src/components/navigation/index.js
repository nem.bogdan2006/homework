import React from "react";
import "./navigation.css"
import User from "../user-info/index"
import NavLinks from "../links/index";
import DownSide from "../down-side/index";


function Navigation() {
    return (
        <div className="nav nav__light-mode">
            <div>
                <User />
                <NavLinks />
            </div>
            <DownSide />
        </div>
    );
}

export default Navigation;