import React from "react";
import { Icon } from '@iconify/react';
import "./nav-links.css"

function NavLinks() {
    return (
        <>
            <div className="link link__light-mode">
                <Icon icon="material-symbols:search" color="#8e8f8f" className="icon-link" />
                <p className="link-text">Search...</p>
            </div>

            <div className="link link__light-mode">
                <Icon icon="teenyicons:house-outline" color="#8e8f8f" className="icon-link" />
                <p className="link-text">Dashboard</p>
            </div>

            <div className="link link__light-mode">
                <Icon icon="mdi:graph-bar" color="#8e8f8f" className="icon-link" />
                <p className="link-text">Revenue</p>
            </div>

            <div className="link link__light-mode">
                <Icon icon="tdesign:notification" color="#8e8f8f" className="icon-link" />
                <p className="link-text">Notification</p>
            </div>

            <div className="link link__light-mode">
                <Icon icon="grommet-icons:analytics" color="#8e8f8f" className="icon-link" />
                <p className="link-text">Analytics</p>
            </div>

            <div className="link link__light-mode">
                <Icon icon="majesticons:box-line" color="#8e8f8f" className="icon-link" />
                <p className="link-text">Inventory</p>
            </div>
        </>
    )
}

export default NavLinks
