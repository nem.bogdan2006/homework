import React from "react";
import { Icon } from '@iconify/react';
import "./down-side.css";



function phBUTTON() {
    const tombol_phBUTTON = document.getElementsByClassName('phBUTTON')[0].classList.toggle('active');
    const
        navMenu = document.querySelector('.nav'),
        userName = document.querySelector('.user-name'),
        navLinks = document.querySelectorAll('.link'),
        logout = document.querySelector('.logout'),
        light = document.querySelectorAll('.light-mode'),
        sun = document.querySelector('.sun'),
        moon = document.querySelector('.moon')


    if (tombol_phBUTTON) {
        navMenu.classList.remove('nav__light-mode');
        navMenu.classList.add('nav__night-mode');

        userName.style.color = '#fff'

        navLinks.forEach(el => {
            el.classList.remove('link__light-mode');
            el.classList.add('link__night-mode');
        })

        logout.classList.remove('logout__light');
        logout.classList.add('logout__night');

        light.forEach(el => {
            el.classList.remove('light-mode__light');
            el.classList.add('light-mode__night');
        })

        sun.classList.remove('hide');
        moon.classList.add('hide')

    } else {
        navMenu.classList.remove('nav__night-mode');
        navMenu.classList.add('nav__light-mode');

        userName.style.color = '#000'

        navLinks.forEach(el => {
            el.classList.remove('link__night-mode');
            el.classList.add('link__light-mode');
        })

        logout.classList.remove('logout__night');
        logout.classList.add('logout__light');

        light.forEach(el => {
            el.classList.remove('light-mode__night');
            el.classList.add('light-mode__light');
        })


        sun.classList.add('hide');
        moon.classList.remove('hide')
    }
}

function DownSide() {
    return (
        <div className="down-side">
            <div className="logout logout__light">
                <Icon icon="material-symbols:logout" color="#8e8f8f" className="icon__down-side" />
                <p className="text__down-side">Logout</p>
            </div>
            <div className="light-mode light-mode__light">
                <Icon icon="ph:moon" color="#8e8f8f" className="moon light-icon icon__down-side" />
                <Icon icon="ph:sun" color="#8e8f8f" className="sun light-icon icon__down-side hide" />
                <p className="text__down-side">Light mode</p>
            </div>
            <span className='phBUTTON' onClick={phBUTTON}>
                <span></span>
            </span>
        </div>
    );
}




export default DownSide;