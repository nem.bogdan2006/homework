import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { req } from "../../methods";
import style from './detail.module.css';
import GoBackBtn from "../goBackBtn";
import MoreInfo from "../moreInfo";

export default function Details({ page }) {

    const [modalMenuDetails, setModalMenuDetails] = useState(false);

    const [information, setInformation] = useState({
        name: null,
        // Character
        status: null,
        species: null,
        gender: null,
        image: null,
        // originUrl: null,
        // locationUrl: null,
        // Location
        type: null,
        dimension: null,
        // Episode
        air_date: null,
        episode: null,

        reqStatus: false,
    })

    // if (page === 'character') {
    //     information.originUrl = null;
    //     information.locationUrl = null;
    // }

    const { id } = useParams();

    useEffect(() => {
        if (!isNaN(id) && id > 0) {
            req(`https://rickandmortyapi.com/api/${page}/${id}`).then(response => {
                let updatedInformation = {
                    name: response.name,
                    // Character
                    status: response.status ?? null,
                    species: response.species ?? null,
                    gender: response.gender ?? null,
                    image: response.image ?? null,
                    // originUrl: response.origin.url ?? null,
                    // locationUrl: response.location.url ?? null,
                    // Location
                    type: response.type ?? null,
                    dimension: response.dimension ?? null,
                    // Episode
                    air_date: response.air_date ?? null,
                    episode: response.episode ?? null,
                    reqStatus: true,

                };

                if (page === 'character') {
                    updatedInformation.originUrl = response.origin.url;
                    updatedInformation.locationUrl = response.location.url;
                }
                // updatedInformation.test=response.origin.url;

                // if ( page === 'location') {
                //     updatedInformation.residentsUrl = response.
                // }


                console.log(updatedInformation);

                setInformation(updatedInformation);
            });
        }
    }, [page, id]);


    return (
        <>
            <GoBackBtn goBack={'/' + page} />
            <div className={style.detailWrapper}>
                {modalMenuDetails ? <MoreInfo page={page} information={information} setModalMenuDetails={setModalMenuDetails} modalMenuDetails={modalMenuDetails} /> : null}
                {information.reqStatus && page === 'character' ?
                    <div className={style.detail__box}>
                        <p> <span className={style.detail__info}>Name:</span> {information.name}    </p>
                        <p> <span className={style.detail__info}>Status</span> Status: {information.status}  </p>
                        <p> <span className={style.detail__info}>Spiecies</span> Spiecies: {information.species} </p>
                        <p> <span className={style.detail__info}>Gender</span> Gender {information.gender}  </p>
                        <img src={information.image} alt={information.name} />
                        <button onClick={() => { setModalMenuDetails(true); }}>More details</button>
                    </div> : null}

                {information.reqStatus && page === 'location' ?
                    <div>
                        <p> <span className={style.detail__info}>Name:</span> {information.name}    </p>
                        <p> <span className={style.detail__info}>type:</span>  {information.type}  </p>
                        <p> <span className={style.detail__info}>dimension:</span> {information.dimension}  </p>
                    </div> : null}

                {information.reqStatus && page === 'episode' ?
                    <div>
                        <p> <span className={style.detail__info}>Name:</span> {information.name}    </p>
                        <p> <span className={style.detail__info}>type:</span>  {information.air_date}  </p>
                        <p> <span className={style.detail__info}>dimension:</span> {information.episode}  </p>
                    </div> : null}
            </div >
        </>
    )
}
