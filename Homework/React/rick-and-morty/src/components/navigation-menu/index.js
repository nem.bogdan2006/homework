import React, { useEffect, useState } from "react";
import Logo from "../../img/logo.svg";
import { req } from "../../methods";
import style from "./navigation-menu.module.css";
import { Link } from "react-router-dom"; 


export default function NavigationMenu (click) {
    const [links, setLinks] = useState([]);

    useEffect(() => {
        const data = req('https://rickandmortyapi.com/api');
        data.then(response => {
            const pages = Object.keys(response);
            setLinks(pages);
        });
    }, []);

    

    return (
        <>
            <div className={style.navMenu}>
                <Link className={style.logo} to={"/"}><img  src={Logo} alt="logo" /></Link>
                <ul className={style.links}>
                    {links.map(el => (
                        <li className={style.link} key={el}>
                            <Link to={`/${el.toLowerCase()}`}>{el}</Link>
                        </li>
                    ))}
                </ul>
            </div>
        </>
    );
}
