import { useEffect, useState } from "react";
import style from "./moreInfo.module.css";
import { req } from "../../methods";

export default function MoreInfo({ page, information, setModalMenuDetails, modalMenuDetails }) {



    const [originInfo, setOriginInfo] = useState({
        name: null,
        dimension: null,
        type: null
    });

    const [location, setlocation] = useState({
        name: null,
        dimension: null,
        type: null,
        created: null
    });

    useEffect(() => {
        if (information.originUrl) {
            req(information.originUrl).then(response => {
                setOriginInfo({
                    name: response.name,
                    dimension: response.dimension,
                    type: response.type
                });
            });
            req(information.locationUrl).then(response => {
                setlocation({
                    name: response.name,
                    dimension: response.dimension,
                    type: response.type,
                    created: response.created
                })
            })
        }
        
    }, [information]);

    return (
        <div className={style.modalMenuBackgound}>
            <div className={style.closeModalMenu} onClick={() => {
                setModalMenuDetails(false);
            }}>❌</div>
            {page === "character" ? (
                <div className={style.modalMenuDetail}>
                    <p className={style.mainText}>Origin</p>
                    <p> <span className={style.modal__info}>name:</span> {originInfo.name} </p>
                    <p> <span className={style.modal__info}>dimension:</span> {originInfo.dimension} </p>
                    <p> <span className={style.modal__info}>type:</span> {originInfo.type} </p>
                    <p className={style.mainText}>Location</p>
                    <p> <span className={style.modal__info}>name:</span> {location.name} </p>
                    <p> <span className={style.modal__info}>dimension:</span> {location.dimension} </p>
                    <p> <span className={style.modal__info}>type:</span> {location.type} </p>
                    <p> <span className={style.modal__info}>created:</span> {location.created} </p>
                </div>
            ) : null}
        </div>
    );
}
