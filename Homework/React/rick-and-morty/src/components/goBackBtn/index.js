import { Link } from "react-router-dom"
import style from "./goBackBtn.module.css"
import Arrow from "../../img/arrow.svg";


export default function GoBackBtn({ goBack }) {


    return (
        <div className={style.goBack}>
            <Link to={ goBack ? goBack + 's' : '/' }>
                <img className={style.arrow} src={Arrow} alt=""></img>
                <p>GO BACK</p>
            </Link>
        </div>
    )
}