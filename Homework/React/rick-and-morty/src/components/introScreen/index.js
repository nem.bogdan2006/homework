import MainImage from "../../img/main-image.svg";
import SearchIcon from "../../img/search-icon.svg";
import style from "./introScreen.module.css";

export default function IntroScreen () {
  return (
    <>
      <div className={style.main__screen}>
        <img className={style.main__image} src={MainImage} alt="RickAndMorty" />
        <div className={style.input__container}>
          <input className={style.main__input} type="text" placeholder="Filter by name or episode (ex. S01 or S01E02)" />
          <img className={style.search__icon} src={SearchIcon} alt="Search" />
        </div>
      </div>
    </>
  );
}
