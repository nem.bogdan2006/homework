import { req } from "../../methods";
import { useEffect, useState } from "react";
import style from "./cards.module.css"
import { Link, useParams } from "react-router-dom";


export default function Cards({ page }) {
    const [exchangeName, setExchangeName] = useState([]);
    const { id } = useParams();

    useEffect(() => {
        const apiBase = `https://rickandmortyapi.com/api/`;
        const apiUrl = id ? `${apiBase}${page}?page=${id}` : `${apiBase}${page}`;

        const data = req(apiUrl);

        data.then(function (info) {
            setExchangeName(info.results);
        });
    }, [id, page]);



    return (
        <div className={style.cards}>
            {exchangeName ? (
                exchangeName.map((info, index) => (
                    <Link to={`/${page}/${info.id}`} key={index}>
                        <div className={style.card} onClick={() => {
                            console.log(index);
                        }}>
                            {page === 'character' ? <img className={style.card__img} src={info.image} alt={page}></img> : null}
                            <div className={style.card__info}>
                                <p className={style.card__name}>{info.name}</p>
                                <p className={style.species}>{info.species}</p>

                                {page === 'location' ? <p> {info.type} </p> : null}
                                {page === 'location' ? <p> {info.dimension} </p> : null}

                                {page === 'episode' ? <p> {info.air_date} </p> : null}
                                {page === 'episode' ? <p> {info.episode} </p> : null}
                            </div>
                        </div>
                    </Link>
                ))
            ) : (
                <p>Loading...</p>
            )}
        </div>
    );

}
