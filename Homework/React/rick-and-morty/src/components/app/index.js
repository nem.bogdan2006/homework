import { BrowserRouter, Route, Routes } from "react-router-dom";
import IntroScreen from "../introScreen";
import ContentScreen from "../contentScreen";
import NotFound from "../notFound";
import NavigationMenu from "../navigation-menu";
import Footer from "../footer";
import Details from "../details";


export default function App() {

    const character = "character";
    const location = "location";
    const episode = "episode";

    return (
        <BrowserRouter>
            <NavigationMenu />
            <Routes>
                <Route path="/" index element={<IntroScreen />} />
                <Route path="/characters" element={<ContentScreen page={character} />}></Route>
                <Route path="/locations" element={<ContentScreen page={location} />}></Route>
                <Route path="/episodes" element={<ContentScreen page={episode} />}></Route>

                {/* Детальний опис персонажа */}
                <Route path="/character/:id" element={<Details page={character}></Details>}></Route>
                <Route path="/location/:id" element={<Details page={location}></Details>}></Route>
                <Route path="/episode/:id" element={<Details page={episode}></Details>}></Route>

                {/* Сторінки з персонажеми, пагінація */}
                <Route path="/character/page/:id" element={<ContentScreen page={character} />} />
                <Route path="/location/page/:id" element={<ContentScreen page={location} />} />
                <Route path="/episode/page/:id" element={<ContentScreen page={episode} />} />

                {/* Not Found Сторінка */}
                <Route path="*" element={<NotFound />}></Route>
            </Routes>
            <Footer></Footer>
        </BrowserRouter>
    );
}
