import style from "./notFound.module.css"

export default function NotFound() {
    return (
        <>
            <div className={style.notFound}>
                <p>Not Found</p>
            </div>
        </>
    )
}

