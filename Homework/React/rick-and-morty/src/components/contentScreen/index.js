import style from "./contentScreen.module.css";
import Cards from "../cards/index";
import GoBackBtn from "../goBackBtn";
import Pagination from "../pagination";


export default function ContentScreen({ page }) {

  
    return (
        <>
            <div className={style.main__menu}>

                <GoBackBtn />
                <Cards page={page} />
                <Pagination page={page} />
            </div>
        </>
    );
}
