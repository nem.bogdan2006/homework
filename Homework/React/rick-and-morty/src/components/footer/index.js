import style from "./footer.module.css"


export default function Footer () {
    

    return (
        <div className={style.footer}> Make with ❤️ for the MobProgramming team</div>
    )
}