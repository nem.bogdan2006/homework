import { useEffect, useState } from "react";
import style from "./pagination.module.css";
import { Link, useParams } from "react-router-dom";
import { req } from "../../methods"

export default function Pagination({ page }) {
    const { id } = useParams();
    const [numPages, setNumPages] = useState();

    useEffect(() => {
        const apiBase = `https://rickandmortyapi.com/api/`;
        const apiUrl = `${apiBase}${page}`;

        const data = req(apiUrl);

        data.then(response => {
            const pages = response.info.pages;
            setNumPages(pages);
        })
    })


    const createPageLinks = () => {
        const pageLinks = [];
        if (id === undefined || id <= 1) {
            pageLinks.push(
                <Link key={1} to={`/${page}/page/1`}>
                    <div className={style.selected__paginationBtn}>1</div>
                </Link>
            );
            pageLinks.push(
                <Link key={2} to={`/${page}/page/2`}>
                    <div className={style.pagination__btns}>2</div>
                </Link>
            );
            pageLinks.push(
                <Link key={3} to={`/${page}/page/3`}>
                    <div className={style.pagination__btns}>3</div>
                </Link>
            );
        } else if (id >= numPages) {
            pageLinks.push(
                <Link key={numPages - 2} to={`/${page}/page/${numPages - 2}`}>
                    <div className={style.pagination__btns}>{numPages - 2}</div>
                </Link>
            );
            pageLinks.push(
                <Link key={numPages - 1} to={`/${page}/page/${numPages - 1}`}>
                    <div className={style.pagination__btns}>{numPages - 1}</div>
                </Link>
            );
            pageLinks.push(
                <Link key={numPages} to={`/${page}s/page/${numPages}`}>
                    <div className={style.selected__paginationBtn}>{numPages}</div>
                </Link>
            );
        } else {
            pageLinks.push(
                <Link key={id - 1} to={`/${page}/page/${id - 1}`}>
                    <div className={style.pagination__btns}>{id - 1}</div>
                </Link>
            );
            pageLinks.push(
                <Link key={id} to={`/${page}/page/${id}`}>
                    <div className={style.selected__paginationBtn}>{id}</div>
                </Link>
            );
            pageLinks.push(
                <Link key={id + 1} to={`/${page}/page/${+id + 1}`}>
                    <div className={style.pagination__btns}>{+id + 1}</div>
                </Link>
            );
        }
        return pageLinks;
    };

    return <div className={style.pagination__box}>{createPageLinks()}</div>;
}