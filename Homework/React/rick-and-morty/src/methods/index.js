

export async function req(url) {
    const rez = await fetch(url);
    return await rez.json();
}

export function showName(info = []) {
    return info.results.map(({ name }) => {
        return <div>{name}</div>
    })
}


export function createValidArray(oldArr) {
    if (Array.isArray(oldArr)) return null;

    let [...arr] = oldArr.results;

    arr = arr.map((obj) => {
        const { id, url, image, category } = searchImg(obj.url)
        return {
            title: obj.name || obj.title,
            id, url, image, category
        }
    })
    return arr
}


export function searchImg(url) {
    let id = "";
    const urls = {
        characters: "https://rickandmortyapi.com/api/character/avatar/",
        location: "https://rickandmortyapi.com/api/location",
        episodes: "https://rickandmortyapi.com/api/episode"
    }
    if (typeof url === "string") {
        id = url.match(/\d+/).join();
    } else {
        throw new Error("url is not string")
    }
    if (url.includes("characters")) {
        return {
            image: urls.characters.image + id + ".jpg",
            id,
            url,
            category: "characters"
        }
    } else if (url.includes("locations")) {
        return {
            image: null,
            id,
            url,
            category: "locations"
        }
    } else if (url.includes("episodes")) {
        return {
            image: null,
            id,
            url,
            category: "episodes"
        }
    }
}
