import React, { Component } from "react";
import "./add-product.css";

class AddProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            productName: "",
            products: [],
            editingProductId: null,
            editedProductName: "",
        };
    }

    createProduct = () => {
        const { productName, products } = this.state;
        if (productName) {
            const newProduct = {
                id: Date.now(),
                name: productName,
                isBought: false,
            };

            this.setState({
                products: [...products, newProduct],
                productName: "",
            });
        }
    };

    handleKeyPress = (e) => {
        if (e.key === "Enter") {
            this.createProduct();
        }
    };

    handleDelete = (productId) => {
        this.setState((prevState) => ({
            products: prevState.products.filter((product) => product.id !== productId),
        }));
    };

    handleEdit = (productId) => {
        this.setState({
            editingProductId: productId,
            editedProductName: this.state.products.find((product) => product.id === productId).name,
        });
    };

    handleSaveEdit = (productId) => {
        this.setState((prevState) => ({
            editingProductId: null,
            products: prevState.products.map((product) =>
                product.id === productId ? { ...product, name: prevState.editedProductName } : product
            ),
        }));
    };

    handleCancelEdit = () => {
        this.setState({
            editingProductId: null,
        });
    };

    handleToggleBought = (productId) => {
        this.setState((prevState) => ({
            products: prevState.products.map((product) =>
                product.id === productId
                    ? { ...product, isBought: !product.isBought }
                    : product
            ),
        }));
    };

    render() {
        const { productName, products, editingProductId, editedProductName } = this.state;

        return (
            <>
                <div className="add-product">
                    <input
                        className="input__add-product"
                        type="text"
                        placeholder="Enter the name of the product"
                        value={productName}
                        onChange={(e) => this.setState({ productName: e.target.value })}
                        onKeyPress={this.handleKeyPress}
                    />
                    <input
                        type="submit"
                        className="btn__add-product"
                        onClick={this.createProduct}
                    />
                </div>
                <ul className="products">
                    {products.map((product) => (
                        <li key={product.id}>
                            {product.id === editingProductId ? (
                                <>
                                    <input
                                        type="text"
                                        className="input__edit"
                                        value={editedProductName}
                                        onChange={(e) => this.setState({ editedProductName: e.target.value })}
                                    />
                                    <div className="btns__edit">
                                        <button className="save__edit btn__edit" onClick={() => this.handleSaveEdit(product.id)}>Save</button>
                                        <button className="cancel__edit btn__edit" onClick={this.handleCancelEdit}>Cancel</button>
                                    </div>
                                </>
                            ) : (
                                <>
                                    {product.name}
                                    <div className="buttons-product">
                                        <button className="btns__product" onClick={() => this.handleDelete(product.id)}>❌</button>
                                        <button className="btns__product" onClick={() => this.handleEdit(product.id)}>✍️</button>
                                        <button className="btns__product" onClick={() => this.handleToggleBought(product.id)}>
                                            {product.isBought ? "✅" : "Куплено"}
                                        </button>
                                    </div>
                                </>
                            )}
                        </li>
                    ))}
                </ul>
            </>
        );
    }
}

export default AddProduct;
