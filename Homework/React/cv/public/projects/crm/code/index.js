// Кнопка додавання товару або відео
const addBtn = document.querySelector('.add');


if (addBtn) {
    addBtn.addEventListener('click', () => document.querySelector('.container-modal').classList.remove('hide'));

}
// Відкривання модального вікна

// Закривання модального вікна 

const closeBtn = document.querySelector('#modal-close');

if (closeBtn) {
    closeBtn.addEventListener('click', () => document.querySelector('.container-modal').classList.add('hide'));
}


// Вибір категорія для додавання продуктів

const category = document.getElementById('product-select');

if (category) {
    category.addEventListener('change', () => {
        const
            selectedOption = category.options[category.selectedIndex],
            selectedText = selectedOption.textContent;

        const productBody = document.querySelector('.product-body')

        const restaurantAndShop = `
                         <label for="name-goods"> Введіть назву продукта
                            <input type="text" id="name-goods">
                        </label>
                        <label for="price"> Введіть ціну 
                            <input type="number" id="price">
                        </label>
                        <label for="goods-description">Опис товару
                            <input type="text" id="goods-description">
                        </label>
                        <button class="save" disabled="true">Зберегти</button>
        `;


        if (selectedText === 'Додати продукт до категорії Ресторан' || selectedText === 'Додати продукт до категорії Магазин') {
            productBody.innerHTML = '';
            productBody.insertAdjacentHTML('beforeend', restaurantAndShop)
        } else if (selectedText === 'Додати продукт до категорії Відео хостинг') {
            productBody.innerHTML = '';
            const video = `
                        <label for="name-video"> Назва відео
                            <input type="text" id="name-video">
                        </label>
                        <label for="video-link"> Посилання на відео
                            <input type="url" name="url" id="url" placeholder="https://example.com" required />
                        </label>
                        <button class="save" disabled="true">Зберегти</button>
        `;
            productBody.insertAdjacentHTML("beforeend", video)
        }


        const inputs = document.querySelectorAll('label input');
        const saveButton = document.querySelector('.save');

        inputs.forEach(input => {
            input.addEventListener('input', (event) => {
                const currentInput = event.target;
                const error = currentInput.parentElement.querySelector('.error');

                if (currentInput.type === 'text') {
                    if (currentInput.value.trim() === '') {
                        if (error) {
                            error.remove();
                        }
                        return;
                    }

                    if (validate(/^[а-яіїйю. ]+$/i, currentInput.value)) {
                        if (error) {
                            error.remove();
                        }
                    } else {
                        if (!error) {
                            const errorDiv = document.createElement('div');
                            errorDiv.classList.add('error');
                            errorDiv.textContent = 'Неправильний формат тексту';
                            currentInput.parentElement.appendChild(errorDiv);
                        }
                    }
                } else if (currentInput.type === 'number') {
                    if (currentInput.value.trim() === '') {
                        if (error) {
                            error.remove();
                        }
                        return;
                    }

                    if (validate(/^[0-9.,%]+$/, currentInput.value)) {
                        if (error) {
                            error.remove();

                        }
                    } else {
                        if (!error) {
                            const errorDiv = document.createElement('div');
                            errorDiv.classList.add('error');
                            errorDiv.textContent = 'Неправильний формат числа';
                            currentInput.parentElement.appendChild(errorDiv);
                        }
                    }
                } else if (currentInput.type === 'url') {
                    if (currentInput.value.trim() === '') {
                        if (error) {
                            error.remove();
                        }
                        return;
                    }

                    if (validate(/(?:(?:https?:\/\/)(?:www)?\.?(?:youtu\.?be)(?:\.com)?\/(?:.*[=/])*)([^= &?/\r\n]{8,11})/, currentInput.value)) {
                        if (error) {
                            error.remove();

                        }
                    } else {
                        if (!error) {
                            const errorDiv = document.createElement('div');
                            errorDiv.classList.add('error');
                            errorDiv.textContent = 'Непарвильне посилання на відео';
                            currentInput.parentElement.appendChild(errorDiv);
                        }
                    }
                }
                const isAllInputsValid = Array.from(inputs).every(input => {
                    return (input.type === 'text' && validate(/^[а-яіїйю. ]+$/i, input.value.trim())) ||
                        (input.type === 'number' && validate(/^[0-9.,%]+$/, input.value.trim())) ||
                        (input.type === 'url' && validate(/(?:(?:https?:\/\/)(?:www)?\.?(?:youtu\.?be)(?:\.com)?\/(?:.*[=/])*)([^= &?/\r\n]{8,11})/, input.value.trim()))
                });

                saveButton.disabled = !isAllInputsValid;


            });




        });


        saveButton.addEventListener('click', () => {

            if (selectedText === 'Додати продукт до категорії Ресторан' || selectedText === 'Додати продукт до категорії Магазин') {
                const
                    name = document.getElementById('name-goods'),
                    price = document.getElementById('price'),
                    description = document.getElementById('goods-description');


                const selectedCategory = selectedText;

                const itemList = JSON.parse(localStorage.getItem(selectedCategory)) || [];

                function generateRandomId(length) {
                    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'\"";
                    let result = "";
                    const charactersLength = characters.length;
                    for (let i = 0; i < length; i++) {
                        result += characters.charAt(Math.floor(Math.random() * charactersLength));
                    }
                    return result;
                }

                const randomId = generateRandomId(8);
                console.log(randomId);


                const newGood = {
                    name: name.value,
                    cost: price.value,
                    description: description.value,
                    dateYear: new Date().getFullYear(),
                    dateMonth: new Date().getMonth() + 1,
                    dateDay: new Date().getDate(),
                    dateHour: new Date().getHours(),
                    dateMinute: new Date().getMinutes(),
                    dateSeconds: new Date().getSeconds(),
                    id: generateRandomId(8),
                    quantity: 0
                };

                itemList.push(newGood);

                localStorage.setItem(selectedCategory, JSON.stringify(itemList));
            } else if (selectedText === 'Додати продукт до категорії Відео хостинг') {
                const nameVideo = document.getElementById('name-video');
                const url = document.getElementById('url');


                const selectedCategory = selectedText;

                const itemList = JSON.parse(localStorage.getItem(selectedCategory)) || [];

                function generateRandomId(length) {
                    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'\"";
                    let result = "";
                    const charactersLength = characters.length;
                    for (let i = 0; i < length; i++) {
                        result += characters.charAt(Math.floor(Math.random() * charactersLength));
                    }
                    return result;
                }

                const randomId = generateRandomId(8);
                console.log(randomId);



                const video = {
                    name: nameVideo.value,
                    url: url.value,
                    dateYear: new Date().getFullYear(),
                    dateMonth: new Date().getMonth() + 1,
                    dateDay: new Date().getDate(),
                    dateHour: new Date().getHours(),
                    dateMinute: new Date().getMinutes(),
                    dateSeconds: new Date().getSeconds(),
                    id: generateRandomId(8),
                };

                itemList.push(video);

                localStorage.setItem(selectedCategory, JSON.stringify(itemList));
            }

        });
        const validate = (r, t) => r.test(t);
    })
}


// Додавання в таблицю продуктів товару з localStorage


const currentURL = window.location.href;
const matches = currentURL.match(/\/([^/]+)\/?$/);
const webPage = matches ? matches[1] : '';


function addNewProduct(category, categoryName) {
    const productStorageJSON = localStorage.getItem(category);
    const products = JSON.parse(productStorageJSON) || [];

    const emptyTable = document.querySelector('.empty-table');
    const tbody = document.querySelector('tbody');

    if (products.length > 0) {
        emptyTable.classList.add('hide');
        tbody.innerHTML = '';

        function updateRowNumbers() {
            const rows = tbody.querySelectorAll('tr');
            rows.forEach((row, index) => {
                const numberCell = row.querySelector('td:first-child');
                numberCell.textContent = index + 1;
            });
        }

        for (let i = 0; i < products.length; i++) {
            const product = products[i];
            const tr = document.createElement('tr');
            const number = document.createElement('td');
            const name = document.createElement('td');
            const residue = document.createElement('td');
            const cost = document.createElement('td');
            const edit = document.createElement('td');
            const status = document.createElement('td');
            const dateOfAddition = document.createElement('td');
            const remove = document.createElement('td');

            number.textContent = i + 1;
            name.textContent = product.name;
            residue.textContent = products[i].quantity;
            cost.textContent = product.cost;

            const editButton = document.createElement('button');
            editButton.textContent = 'Редагувати';
            editButton.classList.add('btnEdit');
            edit.appendChild(editButton);

            if (residue.textContent === '0') {
                status.textContent = '❌';
            } else {
                status.textContent = '✅';
            }



            dateOfAddition.textContent = `${product.dateYear}.${product.dateMonth < 10 ? '0' + product.dateMonth : product.dateMonth}.${product.dateDay < 10 ? '0' + product.dateDay : product.dateDay} ${product.dateHour < 10 ? '0' + product.dateHour : product.dateHour}:${product.dateMinute < 10 ? '0' + product.dateMinute : product.dateMinute}:${product.dateSeconds < 10 ? '0' + product.dateSeconds : product.dateSeconds}`;

            const removeButton = document.createElement('button');
            removeButton.textContent = 'Видалити';
            removeButton.classList.add('btnRemove');
            remove.append(removeButton);

            removeButton.addEventListener('click', () => {
                tbody.removeChild(tr);
                products.splice(i, 1);
                localStorage.setItem(category, JSON.stringify(products));
                updateRowNumbers();

                addNewProduct(category, categoryName);
            });


            tr.appendChild(number);
            tr.appendChild(name);
            tr.appendChild(residue);
            tr.appendChild(cost);
            tr.appendChild(edit);
            tr.appendChild(status);
            tr.appendChild(dateOfAddition);
            tr.appendChild(remove);

            tbody.appendChild(tr);

            // Редагування данних
            const editModalMenu = document.querySelector('.modal-menu__edit');

            editButton.addEventListener('click', () => {
                editModalMenu.classList.remove('hide');

                document.querySelector('.close__modal-menu').addEventListener('click', () => {
                    editModalMenu.classList.add('hide');
                })

                const modalId = document.querySelector('.info-id input');
                const modalName = document.querySelector('.info-name input');
                const modalPrice = document.querySelector('.info-price input');
                const modalDate = document.querySelector('.info-date input');
                const modalDescription = document.querySelector('.info-description input');
                const modalCategory = document.querySelector('.info-category input');
                const modalQuantity = document.querySelector('.info-quantity input');

                if (products[i]) {
                    modalId.value = products[i].id;
                    modalId.disabled = 'true';
                } else {
                    console.error(`Помилка: Об'єкт не існує для індексу ` + i);
                }

                modalName.value = name.textContent;
                modalPrice.value = cost.textContent;
                modalDate.value = dateOfAddition.textContent;
                modalDate.disabled = "true";
                if (products[i] && products[i].description) {
                    modalDescription.value = products[i].description;
                }
                modalCategory.value = categoryName;
                modalCategory.disabled = "true";
                modalQuantity.value = products[i].quantity;

                const saveChange = document.querySelector('.save-change button');
                saveChange.addEventListener('click', () => {

                    products[i].name = modalName.value;
                    products[i].cost = modalPrice.value;
                    products[i].quantity = modalQuantity.value;

                    window.location.reload()

                    if (residue.textContent === '0') {
                        status.textContent = '❌';
                    } else {
                        status.textContent = '✅';
                    }

                    localStorage.setItem(category, JSON.stringify(products));
                    updateRowNumbers();

                    editModalMenu.classList.add('hide');
                });
            });
        }
    } else {
        emptyTable.classList.remove('hide');
    }
}

if ( webPage !== "crm" && webPage !== "index.html") {
    if (webPage === 'store') {
        addNewProduct('Додати продукт до категорії Магазин', 'StoreProduct');
    } else if (webPage === 'restoran') {
        addNewProduct('Додати продукт до категорії Ресторан', 'RestoranProduct');
    } else {
        function addNewVideo() {
            const productStorageJSON = localStorage.getItem('Додати продукт до категорії Відео хостинг');
            const products = JSON.parse(productStorageJSON) || [];
    
            const emptyTable = document.querySelector('.empty-table');
            const tbody = document.querySelector('tbody');
    
            if (products.length > 0) {
                emptyTable.classList.add('hide');
                tbody.innerHTML = '';
    
                function updateRowNumbers() {
                    const rows = tbody.querySelectorAll('tr');
                    rows.forEach((row, index) => {
                        const numberCell = row.querySelector('td:first-child');
                        numberCell.textContent = index + 1;
                    });
                }
    
                for (let i = 0; i < products.length; i++) {
                    const product = products[i];
                    const tr = document.createElement('tr');
                    const number = document.createElement('td');
                    const name = document.createElement('td');
                    const edit = document.createElement('td');
                    const link = document.createElement('td');
                    const dateOfAddition = document.createElement('td');
                    const remove = document.createElement('td');
    
                    number.textContent = i + 1;
                    name.textContent = product.name;
                    link.textContent = products[i].url;
    
                    const editButton = document.createElement('button');
                    editButton.textContent = 'Редагувати';
                    editButton.classList.add('btnEdit');
                    edit.appendChild(editButton);
    
                    dateOfAddition.textContent = `${product.dateYear}.${product.dateMonth < 10 ? '0' + product.dateMonth : product.dateMonth}.${product.dateDay < 10 ? '0' + product.dateDay : product.dateDay} ${product.dateHour < 10 ? '0' + product.dateHour : product.dateHour}:${product.dateMinute < 10 ? '0' + product.dateMinute : product.dateMinute}:${product.dateSeconds < 10 ? '0' + product.dateSeconds : product.dateSeconds}`;
    
                    const removeButton = document.createElement('button');
                    removeButton.textContent = 'Видалити';
                    removeButton.classList.add('btnRemove');
                    remove.append(removeButton);
    
                    removeButton.addEventListener('click', () => {
                        tbody.removeChild(tr);
                        products.splice(i, 1);
                        localStorage.setItem('Додати продукт до категорії Відео хостинг', JSON.stringify(products));
                        updateRowNumbers();
    
                        // Оновлюємо таблицю після видалення
                        addNewVideo();
                    });
    
                    tr.appendChild(number);
                    tr.appendChild(name);
                    tr.appendChild(dateOfAddition);
                    tr.appendChild(link);
                    tr.appendChild(edit);
                    tr.appendChild(remove);
    
                    tbody.appendChild(tr);
    
                    // Редагування данних
                    const editModalMenu = document.querySelector('.modal-menu__edit');
    
                    editButton.addEventListener('click', () => {
                        editModalMenu.classList.remove('hide');
    
                        document.querySelector('.close__modal-menu').addEventListener('click', () => {
                            editModalMenu.classList.add('hide');
                        })
    
                        const modalId = document.querySelector('.info-id input');
                        const modalName = document.querySelector('.info-name input');
                        const modalDate = document.querySelector('.info-date input');
                        const modalDescription = document.querySelector('.info-description input');
                        const modalCategory = document.querySelector('.info-category input');
                        const modalUrl = document.querySelector('.info-link input');
    
                        if (products[i]) {
                            modalId.value = products[i].id;
                            modalId.disabled = 'true';
                        } else {
                            console.error(`Помилка: Об'єкт не існує для індексу ` + i);
                        }
    
                        modalName.value = name.textContent;
                        modalDate.value = dateOfAddition.textContent;
                        modalDate.disabled = "true";
                        if (products[i] && products[i].description) {
                            modalDescription.value = products[i].description;
                        }
                        modalCategory.value = 'VideoCategory';
                        modalCategory.disabled = "true";
                        modalUrl.value = link.textContent;
    
                        const saveChange = document.querySelector('.save-change button');
                        saveChange.addEventListener('click', () => {
                            products[i].name = modalName.value;
                            name.textContent = modalName.value;
    
                            products[i].url = modalUrl.value;
                            link.textContent = modalUrl.value;
                            localStorage.setItem('Додати продукт до категорії Відео хостинг', JSON.stringify(products));
                            updateRowNumbers();
    
                            editModalMenu.classList.add('hide');
                        });
                    });
                }
            } 
            else {
                emptyTable.classList.remove('hide');
            }
        }
        addNewVideo();
    }
    
}
