const goods = {
    shorts: [
        {
            img: "./img/goods/shorts/img1.svg",
            name: "Sportif's Original Short",
            cost: 67.00,
            rate: 5,
            color: "gray",
            id: "ITEM #670170",
            type: 'shorts',
            size: 34,
        },

        {
            img: "./img/goods/shorts/img2.svg",
            name: "Sportif's Hatteras Short",
            cost: 54.99,
            rate: 5,
            color: "white",
            id: "ITEM #670171",
            type: 'shorts',
            size: 36,
        },

        {
            img: "./img/goods/shorts/img3.svg",
            name: "Sportif's Tidewater Short",
            cost: 54.99,
            rate: 5,
            color: "blue",
            id: "ITEM #670172",
            type: 'shorts',
            size: 32,
        },

        {
            img: "./img/goods/shorts/img4.svg",
            name: "Sportif's Lauderdale Short",
            cost: 67.00,
            rate: 5,
            color: "white",
            id: "ITEM #670173",
            type: 'shorts',
            size: 32,
        },

        {
            img: "./img/goods/shorts/img5.svg",
            name: "Captain's Short",
            cost: 67.00,
            rate: 5,
            color: "blue",
            id: "ITEM #670174",
            type: 'shorts',
            size: 40,
        },

        {
            img: "./img/goods/shorts/img6.svg",
            name: "Galapagos Plain Short",
            cost: 34.99,
            rate: 5,
            color: "white",
            id: "ITEM #670175",
            type: 'shorts',
            size: 42,
        },

        {
            img: "./img/goods/shorts/img7.svg",
            name: "Galapagos Pleated Short",
            cost: 34.99,
            rate: 5,
            color: "gray",
            id: "ITEM #670176",
            type: 'shorts',
            size: 34,
        },

        {
            img: "./img/goods/shorts/img8.svg",
            name: "Sportif's Tidewater Denim ",
            cost: 69.00,
            rate: 5,
            color: "blue",
            id: "ITEM #670177",
            type: 'shorts',
            size: 36,
        },

        {
            img: "./img/goods/shorts/img9.svg",
            name: "Marchal's Hatteras Short",
            cost: 40.99,
            rate: 5,
            color: "brown",
            id: "ITEM #670178",
            type: 'shorts',
            size: 44,
        },

        {
            img: "./img/goods/shorts/img10.svg",
            name: "Ecoths Ashcroft Short",
            cost: 41.99,
            rate: 5,
            color: "brown",
            id: "ITEM #670179",
            type: 'shorts',
            size: 38,
        },

        {
            img: "./img/goods/shorts/img11.png",
            name: "Shorts Glo-story MTT-212",
            cost: 56.99,
            rate: 5,
            color: "blue",
            id: "ITEM #670180",
            type: 'shorts',
            size: 46,
        },

        {
            img: "./img/goods/shorts/img12.png",
            name: "Shorts Glo-story MTT-561",
            cost: 56.99,
            rate: 5,
            color: "white",
            id: "ITEM #670165",
            type: 'shorts',
            size: 42,
        },

        {
            img: "./img/goods/shorts/img13.png",
            name: "Shorts Glo-story MIT-143",
            cost: 56.99,
            rate: 5,
            color: "gray",
            id: "ITEM #670181",
            type: 'shorts',
            size: 44,
        },

        {
            img: "./img/goods/shorts/img14.png",
            name: "Shorts Glo-story MLK-71",
            cost: 56.99,
            rate: 5,
            color: "black",
            id: "ITEM #670182",
            type: 'shorts',
            size: 32,
        },

        {
            img: "./img/goods/shorts/img15.png",
            name: "Shorts Glo-story MRT-42",
            cost: 56.99,
            rate: 5,
            color: "gray",
            id: "ITEM #670183",
            type: 'shorts',
            size: 34,
        },

        {
            img: "./img/goods/shorts/img16.png",
            name: "Shorts Glo-story MMK-49",
            cost: 56.99,
            rate: 5,
            color: "brown",
            id: "ITEM #670184",
            type: 'shorts',
            size: 36,
        },

        {
            img: "./img/goods/shorts/img17.png",
            name: "Sports shorts Glo-Story",
            cost: 34.99,
            rate: 5,
            color: "gray",
            id: "ITEM #670185",
            type: 'shorts',
            size: 38,
        },

        {
            img: "./img/goods/shorts/img18.png",
            name: "Shorts Glo-Story MRT-303",
            cost: 49.99,
            rate: 5,
            color: "black",
            id: "ITEM #670186",
            type: 'shorts',
            size: 40,
        },

        {
            img: "./img/goods/shorts/img19.png",
            name: "Denim shorts Bambi",
            cost: 74.00,
            rate: 5,
            color: "blue",
            id: "ITEM #670187",
            type: 'shorts',
            size: 42,
        },

        {
            img: "./img/goods/shorts/img20.png",
            name: "Shorts Joma Jungle",
            cost: 53.00,
            rate: 5,
            color: "blue",
            id: "ITEM #670188",
            type: 'shorts',
            size: 44,
        },

    ],









    pants: [
        {
            img: "./img/goods/trouseres/img1.png",
            name: "Quandary Pants - Regular",
            cost: 49.99,
            rate: 4,
            color: "white",
            id: "ITEM #531400",
            type: 'pants',
            size: 32,
        },

        {
            img: "./img/goods/trouseres/img2.png",
            name: "Pants Marine",
            cost: 67.59,
            rate: 5,
            color: "green",
            id: "ITEM #531401",
            type: 'pants',
            size: 34,
        },

        {
            img: "./img/goods/trouseres/img3.png",
            name: "Puma Ess Sweatpants",
            cost: 78.99,
            rate: 5,
            color: "green",
            id: "ITEM #531402",
            type: 'pants',
            size: 36,
        },

        {
            img: "./img/goods/trouseres/img4.png",
            name: "Tactical cargo pants",
            cost: 49.99,
            rate: 5,
            color: "white",
            id: "ITEM #531403",
            type: 'pants',
            size: 38,
        },

        {
            img: "./img/goods/trouseres/img5.png",
            name: "Tactical pants Olive",
            cost: 39.99,
            rate: 4,
            color: "green",
            id: "ITEM #531404",
            type: 'pants',
            size: 40,
        },

        {
            img: "./img/goods/trouseres/img6.png",
            name: "Men's tactical pants",
            cost: 51.55,
            rate: 5,
            color: "blue",
            id: "ITEM #531405",
            type: 'pants',
            size: 42,
        },

        {
            img: "./img/goods/trouseres/img7.png",
            name: "Men's sports pants DEXT",
            cost: 55.00,
            rate: 5,
            color: "red",
            id: "ITEM #531406",
            type: 'pants',
            size: 44,
        },

        {
            img: "./img/goods/trouseres/img8.png",
            name: "Sports pants for men",
            cost: 54.00,
            rate: 5,
            color: "black",
            id: "ITEM #531407",
            type: 'pants',
            size: 46,
        },

        {
            img: "./img/goods/trouseres/img9.png",
            name: "Men's sports pants",
            cost: 72.99,
            rate: 4,
            color: "blue",
            id: "ITEM #531408",
            type: 'pants',
            size: 30,
        },

        {
            img: "./img/goods/trouseres/img10.png",
            name: "warm sports pants",
            cost: 41.00,
            rate: 5,
            color: "blue",
            id: "ITEM #531409",
            type: 'pants',
            size: 34,
        },

        {
            img: "./img/goods/trouseres/img11.png",
            name: "Sports pants DEMMA",
            cost: 32.99,
            rate: 5,
            color: "gray",
            id: "ITEM #531410",
            type: 'pants',
            size: 36,
        },

        {
            img: "./img/goods/trouseres/img12.png",
            name: "Sports pants United",
            cost: 59.99,
            rate: 5,
            color: "white",
            id: "ITEM #531411",
            type: 'pants',
            size: 38,
        },

        {
            img: "./img/goods/trouseres/img13.png",
            name: "Sports pants United",
            cost: 44.00,
            rate: 5,
            color: "green",
            id: "ITEM #531412",
            type: 'pants',
            size: 40,
        },

        {
            img: "./img/goods/trouseres/img14.png",
            name: "Colin's sweatpants",
            cost: 91.00,
            rate: 4,
            color: "white",
            id: "ITEM #531413",
            type: 'pants',
            size: 42,
        },

        {
            img: "./img/goods/trouseres/img15.png",
            name: "Sports pants set black gray",
            cost: 44.00,
            rate: 5,
            color: "white" || "black",
            id: "ITEM #531414",
            type: 'pants',
            size: 44,
        },

        {
            img: "./img/goods/trouseres/img16.png",
            name: "Sports pants",
            cost: 53.33,
            rate: 5,
            color: "white",
            id: "ITEM #531415",
            type: 'pants',
            size: 32,
        },

        {
            img: "./img/goods/trouseres/img17.png",
            name: "Sports pants",
            cost: 61.99,
            rate: 5,
            color: "blue",
            id: "ITEM #531416",
            type: 'pants',
            size: 34,
        },

        {
            img: "./img/goods/trouseres/img18.png",
            name: "Sports pants ROZA",
            cost: 64.00,
            rate: 5,
            color: "black",
            id: "ITEM #531417",
            type: 'pants',
            size: 36,
        },

        {
            img: "./img/goods/trouseres/img19.png",
            name: "Sports pants DeFacto",
            cost: 53.00,
            rate: 4,
            color: "gray",
            id: "ITEM #531418",
            type: 'pants',
            size: 38,
        },

        {
            img: "./img/goods/trouseres/img20.png",
            name: "DeFacto sweatpants",
            cost: 55.00,
            rate: 5,
            color: "black",
            id: "ITEM #531419",
            type: 'pants',
            size: 40,
        },
    ],











    shirts: [
        {
            img: "./img/goods/shirt/img1.png",
            name: "Roly Beagle T-shirt",
            cost: 29.00,
            rate: 5,
            color: "black",
            id: "ITEM #911451",
            type: 'shirts',
            size: 32,
        },

        {
            img: "./img/goods/shirt/img2.png",
            name: "B&C T-shirt",
            cost: 35.99,
            rate: 5,
            color: "green",
            id: "ITEM #911452",
            type: 'shirts',
            size: 34,
        },

        {
            img: "./img/goods/shirt/img3.png",
            name: "Style Print T-shirt",
            cost: 45.00,
            rate: 5,
            color: "black",
            id: "ITEM #911453",
            type: 'shirts',
            size: 36,
        },

        {
            img: "./img/goods/shirt/img4.png",
            name: "Style Print men's T-shirt",
            cost: 29.99,
            rate: 5,
            color: "black",
            id: "ITEM #911454",
            type: 'shirts',
            size: 38,
        },

        {
            img: "./img/goods/shirt/img5.png",
            name: "Oversized T-shirt",
            cost: 53.00,
            rate: 5,
            color: "white",
            id: "ITEM #911455",
            type: 'shirts',
            size: 40,
        },

        {
            img: "./img/goods/shirt/img6.png",
            name: "Joma Supernova T-shirt",
            cost: 17.00,
            rate: 5,
            color: "black",
            id: "ITEM #911456",
            type: 'shirts',
            size: 42,
        },

        {
            img: "./img/goods/shirt/img7.png",
            name: "Joma Versalles T-shirt",
            cost: 23.53,
            rate: 5,
            color: "blue",
            id: "ITEM #911457",
            type: 'shirts',
            size: 44,
        },

        {
            img: "./img/goods/shirt/img8.png",
            name: "Men's T-shirt Stereotip",
            cost: 44.00,
            rate: 5,
            color: "black",
            id: "ITEM #911458",
            type: 'shirts',
            size: 32,
        },

        {
            img: "./img/goods/shirt/img9.png",
            name: "Fruit of the Loom T-shirt",
            cost: 21.00,
            rate: 5,
            color: "blue",
            id: "ITEM #911459",
            type: 'shirts',
            size: 38,
        },

        {
            img: "./img/goods/shirt/img10.png",
            name: "Spark Camo T-shirt",
            cost: 18.00,
            rate: 5,
            color: "green",
            id: "ITEM #911460",
            type: 'shirts',
            size: 44,
        },


    ],







    accessories: [
        {
            img: "./img/goods/accessories/img1.png",
            name: "Sun protection glasses",
            cost: 10.00,
            rate: 5,
            color: "black",
            id: "ITEM #321156",
            type: 'accessories',
        },

        {
            img: "./img/goods/accessories/img2.png",
            name: "Pillow",
            cost: 7.99,
            rate: 5,
            color: "blue",
            id: "ITEM #321157",
            type: 'accessories',
        },

        {
            img: "./img/goods/accessories/img3.png",
            name: "Sonnenvisor",
            cost: 12.00,
            rate: 5,
            color: "white",
            id: "ITEM #3211558",
            type: 'accessories',
        },

        {
            img: "./img/goods/accessories/img4.png",
            name: "Gloves",
            cost: 14.99,
            rate: 5,
            color: "white",
            id: "ITEM #321159",
            type: 'accessories',
        },

        {
            img: "./img/goods/accessories/img5.png",
            name: "Cap",
            cost: 1,
            rate: 19.99,
            color: "white",
            id: "ITEM #321160",
            type: 'accessories',
        },

        {
            img: "./img/goods/accessories/img6.png",
            name: "Bag",
            cost: 1,
            rate: 39.99,
            color: "green",
            id: "ITEM #321161",
            type: 'accessories',
        },

        {
            img: "./img/goods/accessories/img7.png",
            name: "Suitcase",
            cost: 67.00,
            rate: 5,
            color: "black",
            id: "ITEM #321162",
            type: 'accessories',
        },

        {
            img: "./img/goods/accessories/img8.png",
            name: "Belt",
            cost: 8.99,
            rate: 5,
            color: "blue",
            id: "ITEM #321163",
            type: 'accessories',
        },

        {
            img: "./img/goods/accessories/img9.png",
            name: "Small-bag",
            cost: 14.99,
            rate: 5,
            color: "blue",
            id: "ITEM #321164",
            type: 'accessories',
        },

        {
            img: "./img/goods/accessories/img10.png",
            name: "Umbrella",
            cost: 10.00,
            rate: 5,
            color: "green",
            id: "ITEM #321165",
            type: 'accessories',
        },
    ]
}
