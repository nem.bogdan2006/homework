import style from "./about-me.module.css";
import AboutMeImage from "../../img/about-me.png"

export default function AboutMe() {

    return (
        <>
            <div className="container">
                <div className={style.aboutMe}>
                    <div>
                        <p className={style.mainText__aboutMe}>About me</p>
                        <p className={style.text__aboutMe}>
                            Hi, I'm a front-end developer specializing in React, JavaScript, HTML, and CSS. I have experience in building interactive and
                            attractive websites. My knowledge of English helps me to do research and communicate in the global developer community.
                            I am ready for new projects and opportunities in the field of front-end development."
                        </p>
                        <button className={style.btn__aboutMe}>Resume</button>
                    </div>
                    <img src={AboutMeImage} alt="" />
                </div>
            </div>
        </>
    );

}
