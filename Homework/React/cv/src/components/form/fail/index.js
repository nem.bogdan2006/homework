import style from "./fail.module.css"
import { Link } from "react-router-dom"

export default function Fail() {

    return (
        <>
            <div className={style.fail}>
                <div>
                    <p>The message was not sent. Please try again❌</p>
                    <p className={style.goBack}><Link to="/">Go back</Link></p>
                </div>
            </div>
        </>
    )

}