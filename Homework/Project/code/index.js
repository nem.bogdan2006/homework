
// Bustket

const category = window.location.href.split('/').pop();
const newCategory = category ? category.replace(/\.[^/.]+$/, "") : "";


// Cards GOODS

const
    pageNumMatch = window.location.href.split('?')[1]?.match(/\d+/),

    pageNum = pageNumMatch ? parseInt(pageNumMatch[0]) : 1,
    itemsPerPage = 10,
    startIndex = (pageNum - 1) * itemsPerPage,
    endIndex = startIndex + itemsPerPage;




function showCards() {
    if (newCategory === "shorts") {

        const goodsToShow = goods.shorts.slice(startIndex, endIndex);
        const cardsContainer = document.querySelector(".cards__goods-page");

        for (let i = 0; i < goodsToShow.length; i++) {

            const card = document.createElement("div");
            card.classList.add('card__goods');

            card.setAttribute('data-size', goodsToShow[i].size);
            card.setAttribute('data-color', goodsToShow[i].color);
            const itemId = goodsToShow[i].id;

            const clickedProduct = {};

            card.addEventListener('click', (event) => {
                event.stopPropagation();
                clickedProduct.name = goodsToShow[i].name;
                clickedProduct.size = goodsToShow[i].size;
                clickedProduct.color = goodsToShow[i].color;
                clickedProduct.cost = goodsToShow[i].cost.toFixed(2);
                clickedProduct.img = goodsToShow[i].img;
                clickedProduct.id = itemId;

                localStorage.setItem('clickedProductsStorage', JSON.stringify(clickedProduct));

                createProductCard();
            });





            const imgCard = document.createElement("div");
            imgCard.classList.add("img__card");
            const img = document.createElement("img");
            img.classList.add("img__card");
            img.src = goodsToShow[i].img;
            img.alt = "short";
            imgCard.append(img);
            card.appendChild(imgCard);

            const nameGoods = document.createElement("p");
            nameGoods.classList.add('name__goods');
            nameGoods.textContent = goodsToShow[i].name;
            card.append(nameGoods);

            const stars = document.createElement("img");
            stars.classList.add("stars");
            stars.src = "../img/stars.png";
            stars.alt = "rate";
            card.append(stars);

            const price = document.createElement("p");
            price.classList.add("price__goods");
            price.innerHTML = `As low as <span>$${goodsToShow[i].cost.toFixed(2)}</span>`;
            card.appendChild(price);

            const colorsCard = document.createElement("div");
            colorsCard.classList.add("colors__card");
            card.append(colorsCard);


            const
                colorBlock = document.createElement("div"),
                colorBlock2 = document.createElement("div"),
                colorBlock3 = document.createElement("div"),
                colorBlock4 = document.createElement("div"),
                colorBlack = document.createElement("div"),
                colorBodily = document.createElement("div"),
                colorBrown = document.createElement("div"),
                colorBlue = document.createElement("div"),
                colorGray = document.createElement("div");



            colorsCard.append(colorBlock);
            colorsCard.append(colorBlock2);
            colorsCard.append(colorBlock3);
            colorsCard.append(colorBlock4);


            colorBlock.classList.add("colors__card");
            colorBlock2.classList.add("colors__card");
            colorBlock3.classList.add("colors__card");
            colorBlock4.classList.add("colors__card");



            colorBlack.classList.add("color");
            colorBlack.classList.add("color-black");
            colorBlock.append(colorBlack);

            colorBodily.classList.add("color");
            colorBodily.classList.add("color-bodily");
            colorBlock.append(colorBodily);


            colorBrown.classList.add("color");
            colorBrown.classList.add("color-brown");
            colorBlock.append(colorBrown);


            colorBlue.classList.add("color");
            colorBlue.classList.add("color-blue");
            colorBlock.append(colorBlue);


            colorGray.classList.add("color");
            colorGray.classList.add("color-gray");
            colorBlock.append(colorGray);



            const addButton = document.createElement("button");
            addButton.classList.add("btn__card-goods");
            addButton.innerHTML = `<img src="../img/icon/package.png"> ADD TO CART`;


            addButton.addEventListener('click', function (event) {
                event.stopPropagation();

                const selectedProduct = {
                    name: goodsToShow[i].name,
                    size: goodsToShow[i].size,
                    color: goodsToShow[i].color,
                    cost: goodsToShow[i].cost.toFixed(2),
                    img: goodsToShow[i].img
                };

                const selectedProductsJSON = localStorage.getItem('selectedProducts');
                let selectedProducts = JSON.parse(selectedProductsJSON) || [];

                const existingProduct = selectedProducts.find(product => product.name === selectedProduct.name);
                if (!existingProduct) {
                    selectedProducts.push(selectedProduct);
                    localStorage.setItem('selectedProducts', JSON.stringify(selectedProducts));
                }

                updateCartItemCount();

                addButton.setAttribute('disabled', '');

                window.location.reload();
            });



            function updateCartItemCount() {
                const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts'));
                const numBusketElement = document.querySelector('.number__busket');
                const numItems = selectedProducts ? Object.keys(selectedProducts).length : 0;
                numBusketElement.textContent = numItems;
                numBusketElement.style.display = numItems > 0 ? 'flex' : 'none';
            }
            card.appendChild(addButton);

            cardsContainer.appendChild(card);
        }
    } else if (newCategory === 'pants') {
        const goodsToShow = goods.pants.slice(startIndex, endIndex);
        const cardsContainer = document.querySelector(".cards__goods-page");

        for (let i = 0; i < goodsToShow.length; i++) {

            const card = document.createElement("div");
            card.classList.add('card__goods');

            card.setAttribute('data-size', goodsToShow[i].size);
            card.setAttribute('data-color', goodsToShow[i].color);
            const itemId = goodsToShow[i].id;

            const clickedProduct = {};

            card.addEventListener('click', (event) => {
                event.preventDefault();
                clickedProduct.name = goodsToShow[i].name;
                clickedProduct.size = goodsToShow[i].size;
                clickedProduct.color = goodsToShow[i].color;
                clickedProduct.cost = goodsToShow[i].cost.toFixed(2);
                clickedProduct.img = goodsToShow[i].img;
                clickedProduct.id = itemId;

                localStorage.setItem('clickedProductsStorage', JSON.stringify(clickedProduct));

                createProductCard();
            });



            const imgCard = document.createElement("div");
            imgCard.classList.add("img__card");
            const img = document.createElement("img");
            img.classList.add("img__card");
            img.src = goodsToShow[i].img;
            img.alt = newCategory;
            imgCard.append(img);
            card.appendChild(imgCard);

            const nameGoods = document.createElement("p");
            nameGoods.classList.add('name__goods');
            nameGoods.textContent = goodsToShow[i].name;
            card.append(nameGoods);

            const stars = document.createElement("img");
            stars.classList.add("stars");
            stars.src = "../img/stars.png";
            stars.alt = "rate";
            card.append(stars);

            const price = document.createElement("p");
            price.classList.add("price__goods");
            price.innerHTML = `As low as <span>$${goodsToShow[i].cost.toFixed(2)}</span>`;
            card.appendChild(price);

            const colorsCard = document.createElement("div");
            colorsCard.classList.add("colors__card");
            card.append(colorsCard);


            const
                colorBlock = document.createElement("div"),
                colorBlock2 = document.createElement("div"),
                colorBlock3 = document.createElement("div"),
                colorBlock4 = document.createElement("div"),
                colorBlack = document.createElement("div"),
                colorBodily = document.createElement("div"),
                colorBrown = document.createElement("div"),
                colorBlue = document.createElement("div"),
                colorGray = document.createElement("div");



            colorsCard.append(colorBlock);
            colorsCard.append(colorBlock2);
            colorsCard.append(colorBlock3);
            colorsCard.append(colorBlock4);


            colorBlock.classList.add("colors__card");
            colorBlock2.classList.add("colors__card");
            colorBlock3.classList.add("colors__card");
            colorBlock4.classList.add("colors__card");



            colorBlack.classList.add("color");
            colorBlack.classList.add("color-black");
            colorBlock.append(colorBlack);

            colorBodily.classList.add("color");
            colorBodily.classList.add("color-bodily");
            colorBlock.append(colorBodily);


            colorBrown.classList.add("color");
            colorBrown.classList.add("color-brown");
            colorBlock.append(colorBrown);


            colorBlue.classList.add("color");
            colorBlue.classList.add("color-blue");
            colorBlock.append(colorBlue);


            colorGray.classList.add("color");
            colorGray.classList.add("color-gray");
            colorBlock.append(colorGray);



            const addButton = document.createElement("button");
            addButton.classList.add("btn__card-goods");
            addButton.innerHTML = `<img src="../img/icon/package.png"> ADD TO CART`;


            addButton.addEventListener('click', function (event) {
                event.stopPropagation();

                const selectedProduct = {
                    name: goodsToShow[i].name,
                    size: goodsToShow[i].size,
                    color: goodsToShow[i].color,
                    cost: goodsToShow[i].cost.toFixed(2),
                    img: goodsToShow[i].img
                };

                const selectedProductsJSON = localStorage.getItem('selectedProducts');
                let selectedProducts = JSON.parse(selectedProductsJSON) || [];

                const existingProduct = selectedProducts.find(product => product.name === selectedProduct.name);
                if (!existingProduct) {
                    selectedProducts.push(selectedProduct);
                    localStorage.setItem('selectedProducts', JSON.stringify(selectedProducts));
                }

                updateCartItemCount();

                addButton.setAttribute('disabled', '');

                window.location.reload();
            });



            function updateCartItemCount() {
                const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts')) || [];
                const numBusketElement = document.querySelector('.number__busket');
                numBusketElement.textContent = selectedProducts.length;
                numBusketElement.style.display = selectedProducts.length > 0 ? 'flex' : 'none';
            }
            card.appendChild(addButton);

            cardsContainer.appendChild(card);
        }
    } else if (newCategory === 'shirts') {
        const goodsToShow = goods.shirts.slice(startIndex, endIndex);
        const cardsContainer = document.querySelector(".cards__goods-page");

        for (let i = 0; i < goodsToShow.length; i++) {

            const card = document.createElement("div");
            card.classList.add('card__goods');

            card.setAttribute('data-size', goodsToShow[i].size);
            card.setAttribute('data-color', goodsToShow[i].color);
            const itemId = goodsToShow[i].id;

            const clickedProduct = {};

            card.addEventListener('click', (event) => {
                event.stopPropagation();
                clickedProduct.name = goodsToShow[i].name;
                clickedProduct.size = goodsToShow[i].size;
                clickedProduct.color = goodsToShow[i].color;
                clickedProduct.cost = goodsToShow[i].cost.toFixed(2);
                clickedProduct.img = goodsToShow[i].img;
                clickedProduct.id = itemId;

                localStorage.setItem('clickedProductsStorage', JSON.stringify(clickedProduct));

                createProductCard();
            });



            const imgCard = document.createElement("div");
            imgCard.classList.add("img__card");
            const img = document.createElement("img");
            img.classList.add("img__card");
            img.src = goodsToShow[i].img;
            img.alt = newCategory;
            imgCard.append(img);
            card.appendChild(imgCard);

            const nameGoods = document.createElement("p");
            nameGoods.classList.add('name__goods');
            nameGoods.textContent = goodsToShow[i].name;
            card.append(nameGoods);

            const stars = document.createElement("img");
            stars.classList.add("stars");
            stars.src = "../img/stars.png";
            stars.alt = "rate";
            card.append(stars);

            const price = document.createElement("p");
            price.classList.add("price__goods");
            price.innerHTML = `As low as <span>$${goodsToShow[i].cost.toFixed(2)}</span>`;
            card.appendChild(price);

            const colorsCard = document.createElement("div");
            colorsCard.classList.add("colors__card");
            card.append(colorsCard);


            const
                colorBlock = document.createElement("div"),
                colorBlock2 = document.createElement("div"),
                colorBlock3 = document.createElement("div"),
                colorBlock4 = document.createElement("div"),
                colorBlack = document.createElement("div"),
                colorBodily = document.createElement("div"),
                colorBrown = document.createElement("div"),
                colorBlue = document.createElement("div"),
                colorGray = document.createElement("div");



            colorsCard.append(colorBlock);
            colorsCard.append(colorBlock2);
            colorsCard.append(colorBlock3);
            colorsCard.append(colorBlock4);


            colorBlock.classList.add("colors__card");
            colorBlock2.classList.add("colors__card");
            colorBlock3.classList.add("colors__card");
            colorBlock4.classList.add("colors__card");



            colorBlack.classList.add("color");
            colorBlack.classList.add("color-black");
            colorBlock.append(colorBlack);

            colorBodily.classList.add("color");
            colorBodily.classList.add("color-bodily");
            colorBlock.append(colorBodily);


            colorBrown.classList.add("color");
            colorBrown.classList.add("color-brown");
            colorBlock.append(colorBrown);


            colorBlue.classList.add("color");
            colorBlue.classList.add("color-blue");
            colorBlock.append(colorBlue);


            colorGray.classList.add("color");
            colorGray.classList.add("color-gray");
            colorBlock.append(colorGray);



            const addButton = document.createElement("button");
            addButton.classList.add("btn__card-goods");
            addButton.innerHTML = `<img src="../img/icon/package.png"> ADD TO CART`;


            addButton.addEventListener('click', function (event) {
                event.stopPropagation();

                const selectedProduct = {
                    name: goodsToShow[i].name,
                    size: goodsToShow[i].size,
                    color: goodsToShow[i].color,
                    cost: goodsToShow[i].cost.toFixed(2),
                    img: goodsToShow[i].img
                };

                const selectedProductsJSON = localStorage.getItem('selectedProducts');
                let selectedProducts = JSON.parse(selectedProductsJSON) || [];

                const existingProduct = selectedProducts.find(product => product.name === selectedProduct.name);
                if (!existingProduct) {
                    selectedProducts.push(selectedProduct);
                    localStorage.setItem('selectedProducts', JSON.stringify(selectedProducts));
                }

                updateCartItemCount();

                addButton.setAttribute('disabled', '');

                window.location.reload();
            });



            function updateCartItemCount() {
                const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts')) || [];
                const numBusketElement = document.querySelector('.number__busket');
                numBusketElement.textContent = selectedProducts.length;
                numBusketElement.style.display = selectedProducts.length > 0 ? 'flex' : 'none';
            }
            card.appendChild(addButton);

            cardsContainer.appendChild(card);
        }
    } else if (newCategory === 'accessories') {
        const goodsToShow = goods.accessories.slice(startIndex, endIndex);
        const cardsContainer = document.querySelector(".cards__goods-page");

        for (let i = 0; i < goodsToShow.length; i++) {

            const card = document.createElement("div");
            card.classList.add('card__goods');

            card.setAttribute('data-size', goodsToShow[i].size);
            card.setAttribute('data-color', goodsToShow[i].color);
            const itemId = goodsToShow[i].id;

            const clickedProduct = {};

            card.addEventListener('click', (event) => {
                event.stopPropagation();
                clickedProduct.name = goodsToShow[i].name;
                clickedProduct.size = goodsToShow[i].size;
                clickedProduct.color = goodsToShow[i].color;
                clickedProduct.cost = goodsToShow[i].cost.toFixed(2);
                clickedProduct.img = goodsToShow[i].img;
                clickedProduct.id = itemId;

                localStorage.setItem('clickedProductsStorage', JSON.stringify(clickedProduct));

                createProductCard();
            });



            const imgCard = document.createElement("div");
            imgCard.classList.add("img__card");
            const img = document.createElement("img");
            img.classList.add("img__card");
            img.src = goodsToShow[i].img;
            img.alt = newCategory;
            imgCard.append(img);
            card.appendChild(imgCard);

            const nameGoods = document.createElement("p");
            nameGoods.classList.add('name__goods');
            nameGoods.textContent = goodsToShow[i].name;
            card.append(nameGoods);

            const stars = document.createElement("img");
            stars.classList.add("stars");
            stars.src = "../img/stars.png";
            stars.alt = "rate";
            card.append(stars);

            const price = document.createElement("p");
            price.classList.add("price__goods");
            price.innerHTML = `As low as <span>$${goodsToShow[i].cost.toFixed(2)}</span>`;
            card.appendChild(price);

            const colorsCard = document.createElement("div");
            colorsCard.classList.add("colors__card");
            card.append(colorsCard);


            const
                colorBlock = document.createElement("div"),
                colorBlock2 = document.createElement("div"),
                colorBlock3 = document.createElement("div"),
                colorBlock4 = document.createElement("div"),
                colorBlack = document.createElement("div"),
                colorBodily = document.createElement("div"),
                colorBrown = document.createElement("div"),
                colorBlue = document.createElement("div"),
                colorGray = document.createElement("div");



            colorsCard.append(colorBlock);
            colorsCard.append(colorBlock2);
            colorsCard.append(colorBlock3);
            colorsCard.append(colorBlock4);


            colorBlock.classList.add("colors__card");
            colorBlock2.classList.add("colors__card");
            colorBlock3.classList.add("colors__card");
            colorBlock4.classList.add("colors__card");



            colorBlack.classList.add("color");
            colorBlack.classList.add("color-black");
            colorBlock.append(colorBlack);

            colorBodily.classList.add("color");
            colorBodily.classList.add("color-bodily");
            colorBlock.append(colorBodily);


            colorBrown.classList.add("color");
            colorBrown.classList.add("color-brown");
            colorBlock.append(colorBrown);


            colorBlue.classList.add("color");
            colorBlue.classList.add("color-blue");
            colorBlock.append(colorBlue);


            colorGray.classList.add("color");
            colorGray.classList.add("color-gray");
            colorBlock.append(colorGray);



            const addButton = document.createElement("button");
            addButton.classList.add("btn__card-goods");
            addButton.innerHTML = `<img src="../img/icon/package.png"> ADD TO CART`;


            addButton.addEventListener('click', function (event) {
                event.stopPropagation();

                const selectedProduct = {
                    name: goodsToShow[i].name,
                    size: goodsToShow[i].size,
                    color: goodsToShow[i].color,
                    cost: goodsToShow[i].cost.toFixed(2),
                    img: goodsToShow[i].img
                };

                const selectedProductsJSON = localStorage.getItem('selectedProducts');
                let selectedProducts = JSON.parse(selectedProductsJSON) || [];

                const existingProduct = selectedProducts.find(product => product.name === selectedProduct.name);
                if (!existingProduct) {
                    selectedProducts.push(selectedProduct);
                    localStorage.setItem('selectedProducts', JSON.stringify(selectedProducts));
                }

                updateCartItemCount();

                addButton.setAttribute('disabled', '');

                window.location.reload();
            });



            function updateCartItemCount() {
                const selectedProducts = JSON.parse(localStorage.getItem('selectedProducts')) || [];
                const numBusketElement = document.querySelector('.number__busket');
                numBusketElement.textContent = selectedProducts.length;
                numBusketElement.style.display = selectedProducts.length > 0 ? 'flex' : 'none';
            }
            card.appendChild(addButton);

            cardsContainer.appendChild(card);
        }
    }
}

showCards();


// filter
const sizeBtn = document.querySelectorAll('.size');
const cards = document.querySelectorAll('.card__goods');



sizeBtn.forEach(sizeButton => {
    sizeButton.addEventListener('click', () => {
        sizeButton.classList.toggle('selected__size');

        const selectedSizes = document.querySelectorAll('.selected__size');

        const selectedSizeValues = Array.from(selectedSizes).map(sizeElem => parseInt(sizeElem.textContent));

        if (selectedSizeValues.length === 0) {
            cards.forEach(card => {
                card.style.display = 'flex';
            });
            return;
        }

        const filteredGoods = goods.shorts.filter(item => selectedSizeValues.includes(item.size));

        cards.forEach(card => {
            const cardSize = parseInt(card.getAttribute('data-size'));

            if (filteredGoods.some(item => item.size === cardSize)) {
                card.style.display = 'flex';
            } else {
                card.style.display = 'none';
            }
        });
    });
});

const colorSettings = document.querySelectorAll('.color__setting');

colorSettings.forEach(colorSetting => {
    colorSetting.addEventListener('click', () => {
        colorSetting.classList.toggle('selected__color');

        const selectedColors = document.querySelectorAll('.selected__color');

        const selectedColorIds = Array.from(selectedColors).map(colorElem => colorElem.id);

        if (selectedColorIds.length === 0) {
            cards.forEach(card => {
                card.style.display = 'flex';
            });
            return;
        }

        const filteredGoods = goods.shorts.filter(item => selectedColorIds.includes(item.color));

        cards.forEach(card => {
            const cardColor = card.getAttribute('data-color');

            if (filteredGoods.some(item => item.color === cardColor)) {
                card.style.display = 'flex';
            } else {
                card.style.display = 'none';
            }
        });
    });
});


// filter

// Cards GOODS










if (newCategory !== 'index' && newCategory !== 'shirts' && newCategory !== 'accessories') {
    let newPageNum = parseInt(pageNum);
    if (newPageNum === 1) {
        const div = document.createElement("div");
        const div2 = document.createElement("div");
        const div3 = document.createElement("div");

        div.textContent = newPageNum;
        div2.textContent = newPageNum + 1;
        div3.textContent = '>';

        div.classList.add("box-number");
        div.classList.add("active-page");
        div2.classList.add("box-number");
        div3.classList.add("box-number");

        const boxNextPages = document.querySelector('.page-number');

        boxNextPages.appendChild(div);
        boxNextPages.appendChild(div2);
        boxNextPages.appendChild(div3);

        div2.addEventListener("click", () => document.location = `./${newCategory}.html?page=2`)
        div3.addEventListener("click", () => document.location = `./${newCategory}.html?page=2`)
    } else {
        const div = document.createElement("div");
        const div2 = document.createElement("div");
        const div3 = document.createElement("div");

        div.textContent = '<';
        div2.textContent = newPageNum - 1;
        div3.textContent = newPageNum;

        div.classList.add("box-number");
        div2.classList.add("box-number");
        div3.classList.add("box-number");
        div3.classList.add("active-page");

        const boxNextPages = document.querySelector('.page-number');

        boxNextPages.append(div);
        boxNextPages.append(div2);
        boxNextPages.append(div3);


        div2.addEventListener("click", () => document.location = `./${newCategory}.html?page=1`)
        div.addEventListener("click", () => document.location = `./${newCategory}.html?page=1`)
    }
}





if (newCategory !== 'index') {
    const pageName = document.querySelector('.name-page__text span');
    pageName.textContent = newCategory.toUpperCase();

}





// Bustket

if (newCategory !== 'index') {
    const
        sizeArrow = document.getElementById('size-arrow'),
        sizesdisplay = document.querySelector('.sizes__clothes'),
        colorArrow = document.getElementById('color-arrow'),
        displayColor = document.querySelector('.colors__settings'),
        inseamArrow = document.getElementById('inseam'),
        sustainableFabricArrow = document.getElementById('sustainable-fabric'),
        coletctionsArrow = document.getElementById('collections');

    sizeArrow.classList.add('arrow__up');
    colorArrow.classList.add('arrow__up');



    sizeArrow.addEventListener('click', () => {
        sizesdisplay.classList.toggle('invisible');
        sizeArrow.classList.toggle('arrow__up');
    });

    colorArrow.addEventListener('click', () => {
        displayColor.classList.toggle('invisible');
        colorArrow.classList.toggle('arrow__up');
    });

    inseamArrow.addEventListener('click', () => {
        inseamArrow.classList.toggle('arrow__up');
    });

    sustainableFabricArrow.addEventListener('click', () => {
        sustainableFabricArrow.classList.toggle('arrow__up');
    });

    coletctionsArrow.addEventListener('click', () => {
        coletctionsArrow.classList.toggle('arrow__up');
    });



}





// Search


const searchInput = document.querySelector('.search');
const goodsList = document.getElementById('goods-list');
const categoryList = document.createElement('ul');

categoryList.classList.add('list');
goodsList.appendChild(categoryList);

populateAllGoods();

const darkDisplay = document.querySelector('.dark-display');

searchInput.addEventListener('input', () => {
    const val = searchInput.value.trim().toLowerCase();
    const liItems = categoryList.querySelectorAll('li');

    darkDisplay.style.display = 'block';

    if (val === '') {
        darkDisplay.style.display = 'none';
        goodsList.style.display = 'none';
    } else {
        goodsList.style.display = 'block';
    }

    liItems.forEach(function (li) {
        const text = li.textContent.toLowerCase();
        const mark = insertMark(text, text.indexOf(val), val.length);

        if (text.includes(val)) {
            li.classList.remove('hide');
            li.innerHTML = mark;
        } else {
            li.classList.add('hide');
        }
    });
});

darkDisplay.addEventListener('click', () => {
    darkDisplay.style.display = 'none';
    goodsList.style.display = 'none';
});

function insertMark(string, pos, len) {
    return string.slice(0, pos) + '<mark>' + string.slice(pos, pos + len) + '</mark>' + string.slice(pos + len);
}

function populateAllGoods() {
    for (const category in goods) {
        const categoryGoods = goods[category];

        for (const item of categoryGoods) {
            const linkItem = document.createElement('a');
            const listItem = document.createElement('li');

            listItem.textContent = item.name.toLowerCase();
            linkItem.appendChild(listItem);
            categoryList.appendChild(linkItem);

            if (category === 'shorts') {
                linkItem.href = `./shorts.html?page=1`;
            } else if (category === 'pants') {
                linkItem.href = `./pants.html?page=1`;
            } else if (category === 'shirts') {
                linkItem.href = `./shirts.html?page=1`;
            } else {
                linkItem.href = `./accessories.html?page=1`;
            }

            linkItem.classList.add('hide');

            searchInput.addEventListener('keypress', () => {
                linkItem.classList.remove('hide');
            });
        }
    }
}



// Search



// Busket






function createBusketCard() {
    const selectedProductsJSON = localStorage.getItem('selectedProducts');
    const selectedProducts = JSON.parse(selectedProductsJSON) || [];

    const productsCount = {};


    const numBusketElement = document.querySelector('.number__busket');
    if (selectedProducts.length > 0) {
        numBusketElement.style.display = 'flex';
    } else {
        numBusketElement.style.display = 'none';
    }
    if (selectedProducts.length > 0) {
        let totalCost = 0;
        const modalMenu = document.querySelector('.cards__modal-menu');
        const finalResult = document.querySelector('.price-order');



        for (let i = 0; i < selectedProducts.length; i++) {
            const productName = selectedProducts[i].name;


            if (productsCount[productName]) {
                console.log(`Товар "${productName}" вже присутній у кошику.`);
                continue;
            } else {
                productsCount[productName] = true;

            }

            totalCost += parseFloat(selectedProducts[i].cost);

            const goodsCardModalMenu = document.createElement('div');
            goodsCardModalMenu.classList.add('goods-card__modal-menu');



            const boxImg = document.createElement('div');
            boxImg.classList.add('card-product__picture');
            goodsCardModalMenu.appendChild(boxImg);

            const img = document.createElement('img');
            img.src = selectedProducts[i].img;
            img.alt = "Product Image";
            boxImg.appendChild(img);

            const infoBox = document.createElement('div');
            infoBox.classList.add('info__card-product');
            goodsCardModalMenu.appendChild(infoBox);


            const infoTitle = document.createElement('p');

            infoTitle.classList.add('title__card-product');
            infoTitle.textContent = selectedProducts[i].name;
            infoBox.appendChild(infoTitle);

            const removeProduct = document.createElement('button');
            removeProduct.classList.add('remove__product');
            removeProduct.innerHTML = '&#10005;'
            infoBox.append(removeProduct);

            removeProduct.addEventListener('click', () => {
                const modalMenu = document.querySelector('.cards__modal-menu');
                const cards = modalMenu.querySelectorAll('.goods-card__modal-menu');

                if (cards.length > 0) {
                    const indexToRemove = 0;

                    if (indexToRemove >= 0 && indexToRemove < cards.length) {
                        const selectedProductsJSON = localStorage.getItem('selectedProducts');
                        let selectedProducts = JSON.parse(selectedProductsJSON);

                        if (selectedProducts && selectedProducts.length > 0) {
                            selectedProducts.splice(indexToRemove, 1);
                            localStorage.setItem('selectedProducts', JSON.stringify(selectedProducts));
                        }

                        modalMenu.removeChild(cards[indexToRemove]);
                    }
                }




                updateRemainingItemsCount();




                updateTotalCost();


                const selectedProductsJSON = localStorage.getItem('selectedProducts');
                const selectedProducts = JSON.parse(selectedProductsJSON);
                numBusketElement.textContent = selectedProducts.length;
            });


            const priceButtons = document.createElement('div');
            priceButtons.classList.add('price__buttons');
            infoBox.appendChild(priceButtons);

            const numGoods = document.createElement('div');
            numGoods.classList.add('btns-number__goods');
            priceButtons.appendChild(numGoods);

            const btnMinus = document.createElement('button');
            btnMinus.classList.add('btn-number__minus');
            btnMinus.textContent = '-';
            numGoods.appendChild(btnMinus);

            const numInput = document.createElement('input');
            numInput.classList.add('no-spinner');
            numInput.classList.add('num__goods');
            numInput.type = 'number';
            numInput.value = '1';
            numInput.min = '1';
            numInput.max = '99';
            numGoods.appendChild(numInput);

            const btnPlus = document.createElement('button');
            btnPlus.classList.add('btn-number__plus');
            btnPlus.textContent = '+';
            numGoods.appendChild(btnPlus);




            const price = document.createElement('div');
            price.classList.add('price__modal-menu');
            priceButtons.appendChild(price);

            const priceText = document.createElement('p');
            priceText.textContent = `$${selectedProducts[i].cost}`;
            price.appendChild(priceText);


            const initialCost = parseFloat(selectedProducts[i].cost);

            function updatePrice() {
                const numValue = parseFloat(numInput.value);
                priceText.textContent = `$${(initialCost * numValue).toFixed(2)}`;
                updateTotalCost();
            }
            btnMinus.addEventListener('click', () => {
                if (numInput.value > numInput.min) {
                    numInput.value = parseFloat(numInput.value) - 1;
                }
                updatePrice();
            });

            btnPlus.addEventListener('click', () => {
                const currentValue = parseFloat(numInput.value);
                if (currentValue < parseFloat(numInput.max)) {
                    numInput.value = (currentValue + 1).toString();
                }
                updatePrice();
            });

            updatePrice();

            modalMenu.appendChild(goodsCardModalMenu);


            if (totalCost === 0) {
                finalResult.textContent = '$0.00';
            }



            function updateRemainingItemsCount() {
                const modalMenu = document.querySelector('.cards__modal-menu');
                const remainingItemsCount = modalMenu.querySelectorAll('.goods-card__modal-menu').length;

                if (remainingItemsCount === 0) {
                    const emptyBusketMenu = document.createElement('div');
                    emptyBusketMenu.classList.add('empty-busket__modal-menu');

                    const emptyBusketImg = document.createElement('img');
                    emptyBusketImg.classList.add('empty-busket');
                    emptyBusketImg.src = '../img/icon/empty-busket.svg';

                    emptyBusketMenu.append(emptyBusketImg);

                    const emptyBusketText1 = document.createElement('p');
                    emptyBusketText1.classList.add('text__empty-busket-1');
                    emptyBusketText1.textContent = 'Basket is empty';

                    const emptyBusketText2 = document.createElement('p');
                    emptyBusketText2.classList.add('text__empty-busket-2');
                    emptyBusketText2.textContent = 'but it can be fixed';

                    emptyBusketMenu.append(emptyBusketText1);
                    emptyBusketMenu.append(emptyBusketText2);

                    modalMenu.innerHTML = '';
                    modalMenu.append(emptyBusketMenu);
                }
            }
            if (totalCost === 0) {
                finalResult.textContent = '$0.00';
            }


        }

        function updateTotalCost() {
            totalCost = 0;
            const cards = document.querySelectorAll('.goods-card__modal-menu');

            cards.forEach(card => {
                const priceText = card.querySelector('.price__modal-menu p');
                totalCost += parseFloat(priceText.textContent.slice(1));
            });

            finalResult.textContent = `$${totalCost.toFixed(2)}`;
        }




        finalResult.textContent = `$${totalCost.toFixed(2)}`;
        updateTotalCost();


        const numBusketElement = document.querySelector('.number__busket');

        numBusketElement.style.display = 'flex';


        numBusketElement.textContent = selectedProducts.length;

    } else {


        const modalMenu = document.querySelector('.cards__modal-menu');



        const emptyBusketMenu = document.createElement('div');
        emptyBusketMenu.classList.add('empty-busket__modal-menu');

        const emptyBusketImg = document.createElement('img');
        emptyBusketImg.classList.add('empty-busket');
        emptyBusketImg.src = '../img/icon/empty-busket.svg';

        emptyBusketMenu.append(emptyBusketImg);

        const
            emptyBusketText1 = document.createElement('p'),
            emptyBusketText2 = document.createElement('p');

        emptyBusketText1.classList.add('text__empty-busket-1');
        emptyBusketText2.classList.add('text__empty-busket-2');

        emptyBusketText1.textContent = 'Busket is empty';
        emptyBusketText2.textContent = 'but it can be fixed';

        emptyBusketMenu.append(emptyBusketText1);
        emptyBusketMenu.append(emptyBusketText2);


        modalMenu.append(emptyBusketMenu);

    }
}

createBusketCard();
















// Open and close Buttons


const
    busketModalMenu = document.querySelector(".modal-menu__busket"),
    btnClose = document.querySelector(".close"),
    busket = document.querySelector(".busket");


busket.addEventListener("click", () => {

    busketModalMenu.style.display = "flex";
});

btnClose.addEventListener("click", () => {
    busketModalMenu.style.display = "none";
    window.addEventListener('load', () => {
        updateRemainingItemsCount();
    });

});





// Open and close Buttons









const goodsPage = document.querySelector('.goods__web-page');
cards.forEach(card => {
    card.addEventListener('click', () => {
        goodsPage.style.display = 'flex';
    });
});

function createProductCard() {

    const clickedProductsStorageJSON = localStorage.getItem('clickedProductsStorage');
    const clickedProduct = JSON.parse(clickedProductsStorageJSON) || {};



    const closeProductPage = document.createElement('span');
    closeProductPage.classList.add('web-page__close');
    closeProductPage.textContent = '×';

    closeProductPage.addEventListener('click', () => {
        goodsPage.innerHTML = '';
        goodsPage.style.display = 'none';
        localStorage.setItem('goodsPageClosed', 'true');
        window.location.reload();
    });

    if (goodsPage) {
        goodsPage.append(closeProductPage);
    }
    const goodsReview = document.createElement('div');
    goodsReview.classList.add('goods-review');

    if (goodsPage) {
        goodsPage.append(goodsReview);
    }

    const imagesReview = document.createElement('div');
    imagesReview.classList.add('images__review');

    goodsReview.append(imagesReview);

    const productImg = document.createElement('div');
    productImg.classList.add('img__goods');

    imagesReview.append(productImg);

    const mainImg = document.createElement('img');
    mainImg.src = clickedProduct.img;

    productImg.append(mainImg);

    const productFromOtherSide = document.createElement('div');
    productFromOtherSide.classList.add('goods__from-other-side');

    imagesReview.append(productFromOtherSide);

    const
        boxOtherSide1 = document.createElement('div'),
        boxOtherSide2 = document.createElement('div'),


        imgOtherSide1 = document.createElement('img'),
        imgOtherSide2 = document.createElement('img');


    boxOtherSide1.classList.add('box__other-side');
    boxOtherSide2.classList.add('box__other-side');

    productFromOtherSide.append(boxOtherSide1);
    productFromOtherSide.append(boxOtherSide2);

    imgOtherSide1.src = clickedProduct.img;
    imgOtherSide2.src = clickedProduct.img;

    boxOtherSide1.append(imgOtherSide1);
    boxOtherSide2.append(imgOtherSide2);



    const infoGoods = document.createElement('div');
    infoGoods.classList.add('info__goods');

    goodsReview.append(infoGoods);

    const productName = document.createElement('p');
    const numItem = document.createElement('span');


    productName.classList.add('name-goods');
    productName.textContent = clickedProduct.name;

    productName.append(numItem)

    numItem.classList.add('number__item');
    numItem.textContent = clickedProduct.id;

    infoGoods.append(productName)


    const
        rateProduct = document.createElement('div'),
        stars = document.createElement('img'),
        rateText = document.createElement('p');

    rateProduct.classList.add('rate');

    stars.src = '../img/stars.png';

    rateText.classList.add('rate__text');
    rateText.textContent = '93 REVIEWS';


    rateProduct.append(stars);
    rateProduct.append(rateText);

    infoGoods.append(rateProduct);


    const
        priceInfo = document.createElement('div'),
        priceText = document.createElement('span'),
        priceProduct = document.createElement('p');

    priceInfo.classList.add('price-info');

    priceText.classList.add('text__price');
    priceText.textContent = 'As low as'

    priceProduct.classList.add('price');
    priceProduct.textContent = '$' + clickedProduct.cost;

    priceInfo.append(priceText);
    priceInfo.append(priceProduct);

    infoGoods.append(priceInfo);

    const colorGoodsPage = document.createElement('div');
    colorGoodsPage.classList.add('color__goods-page');


    const colorText = document.createElement('p');
    colorText.textContent = 'COLOR:'

    colorGoodsPage.append(colorText);

    const colorsInfo = document.createElement('div');
    colorsInfo.classList.add('colors__info');

    const
        clBlack = document.createElement('div'),
        clBodily = document.createElement('div'),
        clBrown = document.createElement('div'),
        clBLue = document.createElement('div'),
        clGray = document.createElement('div');

    clBlack.classList.add('color__info');
    clBodily.classList.add('color__info');
    clBrown.classList.add('color__info');
    clBLue.classList.add('color__info');
    clGray.classList.add('color__info');

    clBlack.classList.add('color-black');
    clBodily.classList.add('color-bodily');
    clBrown.classList.add('color-brown');
    clBLue.classList.add('color-blue');
    clGray.classList.add('color-gray');

    colorsInfo.append(clBlack);
    colorsInfo.append(clBodily);
    colorsInfo.append(clBrown);
    colorsInfo.append(clBLue);
    colorsInfo.append(clGray);


    colorGoodsPage.append(colorsInfo);

    infoGoods.append(colorGoodsPage);

    const sizeGoodsPage = document.createElement('div');
    sizeGoodsPage.classList.add('size__goods-page');

    infoGoods.append(sizeGoodsPage);

    const textSize = document.createElement('p');
    textSize.textContent = 'SIZE';

    sizeGoodsPage.append(textSize);

    const sizesInfo = document.createElement('div');
    sizesInfo.classList.add('sizes__info');

    const
        sizeInfo = document.createElement('div'),
        sizeInfo2 = document.createElement('div'),
        sizeInfo3 = document.createElement('div'),
        sizeInfo4 = document.createElement('div'),
        sizeInfo5 = document.createElement('div'),
        sizeInfo6 = document.createElement('div'),
        sizeInfo7 = document.createElement('div'),
        sizeInfo8 = document.createElement('div'),
        sizeInfo9 = document.createElement('div');

    sizeInfo.classList.add('size__info');
    sizeInfo2.classList.add('size__info');
    sizeInfo3.classList.add('size__info');
    sizeInfo4.classList.add('size__info');
    sizeInfo5.classList.add('size__info');
    sizeInfo6.classList.add('size__info');
    sizeInfo7.classList.add('size__info');
    sizeInfo8.classList.add('size__info');
    sizeInfo9.classList.add('size__info');

    sizeInfo.textContent = 30;
    sizeInfo2.textContent = 32;
    sizeInfo3.textContent = 34;
    sizeInfo4.textContent = 36;
    sizeInfo5.textContent = 38;
    sizeInfo6.textContent = 40;
    sizeInfo7.textContent = 42;
    sizeInfo8.textContent = 44;
    sizeInfo9.textContent = 46;

    sizesInfo.append(sizeInfo);
    sizesInfo.append(sizeInfo2);
    sizesInfo.append(sizeInfo3);
    sizesInfo.append(sizeInfo4);
    sizesInfo.append(sizeInfo5);
    sizesInfo.append(sizeInfo6);
    sizesInfo.append(sizeInfo7);
    sizesInfo.append(sizeInfo8);
    sizesInfo.append(sizeInfo9);

    sizeGoodsPage.append(sizesInfo);





    const buttonsInfo = document.createElement('div');
    buttonsInfo.classList.add('buttons_info');

    infoGoods.append(buttonsInfo);

    const btnAddToBag = document.createElement('div');
    btnAddToBag.classList.add('btn__goods');
    btnAddToBag.classList.add('btn__addToBag');

    buttonsInfo.append(btnAddToBag);
    btnAddToBag.innerHTML = ' <img class="icon__btn" src="../img/icon/package2.svg">ADD TO BAG';

    btnAddToBag.addEventListener('click', function (event) {
        event.stopPropagation();

        btnAddToBag.classList.add('added');

        btnAddToBag.innerHTML = ' <img class="icon__btn" src="../img/icon/package2.svg">ADDED';

        

        const selectedProduct = {
            name: clickedProduct.name,
            size: clickedProduct.size,
            color: clickedProduct.color,
            cost: clickedProduct.cost,
            img: clickedProduct.img
        };

        const selectedProductsJSON = localStorage.getItem('selectedProducts');
        let selectedProducts = JSON.parse(selectedProductsJSON) || [];

        const existingProduct = selectedProducts.find(product => product.name === selectedProduct.name);
        if (!existingProduct) {
            selectedProducts.push(selectedProduct);
            localStorage.setItem('selectedProducts', JSON.stringify(selectedProducts));
        }


        btnAddToBag.setAttribute('disabled', '');
    });



    const btnAddToWishList = document.createElement('div');
    btnAddToWishList.classList.add('btn__goods');
    btnAddToWishList.classList.add('btn__wishList');

    btnAddToWishList.innerHTML = '<img class="icon__btn" src="../img/icon/heart__wishList.png"> ADD TO WISH LIST'

    buttonsInfo.append(btnAddToWishList);

    const socialMedia = document.createElement('div');
    socialMedia.classList.add('social__media');

    infoGoods.append(socialMedia);

    const
        facebook = document.createElement('img'),
        twitter = document.createElement('img'),
        pinterest = document.createElement('img'),
        link = document.createElement('img');

    facebook.src = '../img/icon/facebook.png';
    twitter.src = '../img/icon/twitter.png';
    pinterest.src = '../img/icon/pinterest.png';
    link.src = '../img/icon/link.png';

    socialMedia.append(facebook);
    socialMedia.append(twitter);
    socialMedia.append(pinterest);
    socialMedia.append(link);

    const parcelInfo = document.createElement('div');
    parcelInfo.classList.add('parcel__info');

    infoGoods.append(parcelInfo);

    const titleParcel = document.createElement('p');
    titleParcel.classList.add('title__parcel');
    titleParcel.textContent = '- Worry Free Shopping -';

    parcelInfo.append(titleParcel)

    const infoOfParcel = document.createElement('div');
    infoOfParcel.classList.add('info');

    parcelInfo.append(infoOfParcel);

    const deliverySpeed = document.createElement('div');
    deliverySpeed.classList.add('delivery__speed');

    infoOfParcel.append(deliverySpeed);

    const carImg = document.createElement('img');
    carImg.classList.add('icon__info');
    carImg.src = '../img/icon/car.png';

    const deliverySpeedText = document.createElement('p');
    deliverySpeedText.textContent = 'FREE PRIORITY SHIPPING ON ORDERS $99+*'

    deliverySpeed.append(carImg);
    deliverySpeed.append(deliverySpeedText);

    const freeReturns = document.createElement('div');
    freeReturns.classList.add('free__returns');

    infoOfParcel.append(freeReturns);

    const boxImg = document.createElement('img');
    boxImg.classList.add('icon__info');
    boxImg.src = '../img/icon/box.png';

    const textFreeReturns = document.createElement('p');
    textFreeReturns.textContent = 'FREE RETURNS & EXCHANGES*';

    freeReturns.append(boxImg);
    freeReturns.append(textFreeReturns);


    console.log("Функція createProductCard виконується");

}








// Busket




//  Main Page



let clickedProduct = {};

const cardDiscount = document.querySelectorAll('.card__discount');

cardDiscount.forEach((card) => {
    card.addEventListener('click', (event) => {
        const name = card.querySelector('.name__product').textContent;
        const img = card.querySelector('.discount__photo').src;
        const cost = parseFloat(card.querySelector('.price__discount').textContent.slice(1));
        const id = card.querySelector('.ID-product').textContent;

        const clickedProduct = {
            name: name,
            cost: cost.toFixed(2),
            img: img,
            id: id
        };

        localStorage.setItem('clickedProductsStorage', JSON.stringify(clickedProduct));

        const goodsPage = document.querySelector('.goods__web-page');
        goodsPage.style.display = 'flex';

        createProductCard();
    });
});

const btnMain = document.querySelectorAll('.btn__discount');

btnMain.forEach(btn => {
    btn.addEventListener('click', (event) => {
        event.stopPropagation(); // Зупинити подальше розповсюдження події

        const card = btn.closest('.card__discount');
        const name = card.querySelector('.name__product').textContent;
        const img = card.querySelector('.discount__photo').src;
        const cost = parseFloat(card.querySelector('.price__discount').textContent.slice(1));
        const id = card.querySelector('.ID-product').textContent;

        const selectedProduct = {
            name: name,
            cost: cost.toFixed(2),
            img: img,
            id: id
        };

        const selectedProductsJSON = localStorage.getItem('selectedProducts');
        let selectedProducts = JSON.parse(selectedProductsJSON) || [];

        const existingProduct = selectedProducts.find(product => product.name === selectedProduct.name);
        if (!existingProduct) {
            selectedProducts.push(selectedProduct);
            localStorage.setItem('selectedProducts', JSON.stringify(selectedProducts));
        }

        btn.setAttribute('disabled', '');

        window.location.reload();
    });
});




